import config

from unittest import TestCase
from unittest.mock import Mock

from injector import Injector, singleton

from app.applications.state_machine.delete_task import DeleteTask
from app.applications.state_machine.uninstall_content_task import UninstallContentTask
from app.applications.state_machine.uninstall_task import UninstallTask
from app.applications.state_machine.upgrade_content_task import UpgradeContentTask
from app.core.keys import Configuration
from app.applications import ApplicationRepository
from app.applications.models import InstalledApplication, ApplicationState, InstalledContent, ContentState
from app.applications.state_machine.download_task import DownloadTask
from app.applications.state_machine.install_content_task import InstallContentTask
from app.applications.state_machine.install_task import InstallTask
from app.applications.state_machine.state_machine import StateMachine


class StateMachineTest(TestCase):

    def setUp(self):
        self.application_repository_mock = Mock(spec=ApplicationRepository)

        self.downloadTaskMock = Mock(spec=DownloadTask)
        self.installTaskMock = Mock(spec=InstallTask)
        self.uninstallTaskMock = Mock(spec=UninstallTask)
        self.installContentTaskMock = Mock(spec=InstallContentTask)
        self.uninstallContentTaskMock = Mock(spec=UninstallContentTask)
        self.upgradeContentTaskMock = Mock(spec=UpgradeContentTask)
        self.deleteTaskMock = Mock(spec=DeleteTask)

        def injector_conf(binder):
            binder.bind(DownloadTask, to=self.downloadTaskMock, scope=singleton)
            binder.bind(InstallTask, to=self.installTaskMock, scope=singleton)
            binder.bind(UninstallTask, to=self.uninstallTaskMock, scope=singleton)
            binder.bind(InstallContentTask, to=self.installContentTaskMock, scope=singleton),
            binder.bind(UninstallContentTask, to=self.uninstallContentTaskMock, scope=singleton)
            binder.bind(DeleteTask, to=self.deleteTaskMock, scope=singleton)
            binder.bind(UpgradeContentTask, to=self.upgradeContentTaskMock, scope=singleton)
            binder.bind(Configuration, to=config, scope=singleton)

        self.injector = Injector([injector_conf])

        self.state_machine = StateMachine(self.application_repository_mock, self.injector)

    def test_ping_with_no_task(self):
        #given
        self.application_repository_mock.find_installed_app_with_different_state_or_version.return_value = \
            [InstalledApplication("test.app",current_state=ApplicationState.uninstalled,
                                 target_state=ApplicationState.downloaded,
                                  current_version="1.0.0", target_version="1.0.0")]
        # when
        self.state_machine.ping()

        # then
        self.downloadTaskMock.set_bundle.assert_called_once_with("test.app")
        self.application_repository_mock.find_installed_app_with_different_state_or_version.assert_called_once_with()
        self.downloadTaskMock.start.assert_called_once_with()
        self.assertIs(self.downloadTaskMock, self.state_machine.currentTask)
        self.assertFalse(self.state_machine.taskMutex.locked())

    def test_ping_already_locked(self):
        # given
        self.application_repository_mock.find_installed_app_with_different_state_or_version.return_value = \
            [InstalledApplication("test.app", current_state=ApplicationState.uninstalled,
                                 target_state=ApplicationState.downloaded, target_version="1.0.0")]
        self.state_machine.taskMutex.acquire()

        # when
        self.state_machine.ping()

        # then
        self.downloadTaskMock.start.assert_not_called()

    def test_ping_downloaded_to_installed(self):
        #given
        self.application_repository_mock.find_installed_app_with_different_state_or_version.return_value = \
            [InstalledApplication("test.app",current_state=ApplicationState.downloaded,
                                 target_state=ApplicationState.installed, target_version="1.0.0")]
        # when
        self.state_machine.ping()

        # then
        self.installTaskMock.start.assert_called_once_with()

    def test_ping_downloaded_with_different_version(self):
        """
        Test that no task is launched when an application is in current & target state downloaded but a different
        version (because not installed), and that the next task is triggered
        """
        # given
        self.application_repository_mock.find_installed_app_with_different_state_or_version.return_value = \
            [InstalledApplication("test.app", current_state=ApplicationState.downloaded,
                                  target_state=ApplicationState.downloaded, target_version="1.0.0"),
             InstalledApplication("test.app.2", current_state=ApplicationState.uninstalled,
                                  target_state=ApplicationState.downloaded, target_version="1.0.0")
             ]
        self.application_repository_mock.find_content_with_different_state_or_different_version.return_value = []

        # when
        self.state_machine.ping()

        # then
        self.downloadTaskMock.set_bundle.assert_called_once_with("test.app.2")
        self.downloadTaskMock.start.assert_called_once_with()


    def test_ping_uninstalled_to_installed(self):
        #given
        self.application_repository_mock.find_installed_app_with_different_state_or_version.return_value = \
            [InstalledApplication("test.app",current_state=ApplicationState.uninstalled,
                                 target_state=ApplicationState.installed, target_version="1.0.0")]
        # when
        self.state_machine.ping()

        # then
        self.downloadTaskMock.start.assert_called_once_with()

    def test_ping_uninstalled_to_installed(self):
        # given
        self.application_repository_mock.find_installed_app_with_different_state_or_version.return_value = \
            [InstalledApplication("test.app", current_state=ApplicationState.uninstalled,
                                  target_state=ApplicationState.installed, target_version="1.0.0")]
        # when
        self.state_machine.ping()

        # then
        self.downloadTaskMock.start.assert_called_once_with()

    def test_ping_installed_to_downloaded(self):
        # given
        self.application_repository_mock.find_installed_app_with_different_state_or_version.return_value = \
            [InstalledApplication("test.app", current_state=ApplicationState.installed,
                                  target_state=ApplicationState.downloaded, target_version="1.0.0")]
        # when
        self.state_machine.ping()

        # then
        self.uninstallTaskMock.start.assert_called_once_with()

    def test_ping_downloaded_to_uninstalled(self):
        # given
        self.application_repository_mock.find_installed_app_with_different_state_or_version.return_value = \
            [InstalledApplication("test.app", current_state=ApplicationState.downloaded,
                                  target_state=ApplicationState.uninstalled, target_version="1.0.0")]
        # when
        self.state_machine.ping()

        # then
        self.deleteTaskMock.start.assert_called_once_with()

    def test_ping_installed_to_uninstalled(self):
        # given
        self.application_repository_mock.find_installed_app_with_different_state_or_version.return_value = \
            [InstalledApplication("test.app", current_state=ApplicationState.installed,
                                  target_state=ApplicationState.uninstalled, target_version="1.0.0")]
        # when
        self.state_machine.ping()

        # then
        self.uninstallTaskMock.start.assert_called_once_with()

    def test_ping_version_change(self):
        # given
        self.application_repository_mock.find_installed_app_with_different_state_or_version.return_value = \
            [InstalledApplication("test.app", current_state=ApplicationState.installed,
                                  target_state=ApplicationState.installed,
                                  current_version="1.0.0", target_version="2.0.0")]
        # when
        self.state_machine.ping()

        # then
        self.uninstallTaskMock.set_bundle.assert_called_once_with("test.app")
        self.uninstallTaskMock.set_chained_task.assert_called_once_with(self.deleteTaskMock)
        self.uninstallTaskMock.start.assert_called_once_with()
