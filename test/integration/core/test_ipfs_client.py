from unittest import TestCase

from app.core.ipfs_client import IpfsClient
import config


class IpfsClientTestCase(TestCase):

    def setUp(self):
        self.ipfs_client = IpfsClient(config)

    def _ipfs_pin_ls_contains(self, hash):

        keys = self.ipfs_client._api().pin_ls()

        return hash in keys['Keys']
