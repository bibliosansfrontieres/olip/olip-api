import os
from urllib.parse import urlparse

from authlib.flask.oauth2 import AuthorizationServer, ResourceProtector
import authlib.specs.oidc.grants as grants
from authlib.specs.rfc6750 import BearerTokenValidator
from flask import session, request, render_template
from injector import inject, Module, provider, singleton

import config
from app.applications import ApplicationService
from app.applications.models import InstalledApplication, Application
from app.core.keys import App, Configuration
from app.users import UserService

from app.tools.request_tools import get_hostname


class OidcBearerTokenValidator(BearerTokenValidator):

    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def authenticate_token(self, token_string):
        return self.user_service.validate_token(token_string)

    def request_invalid(self, request):
        return False

    def token_revoked(self, token):
        return token.revoked


def current_user(user_service: UserService):

    if 'username' in session:
        username = session['username']
        provider = None

        if 'provider' in session:
            provider = session['provider']

        return user_service.get_by_username_and_provider(username, provider=provider)

    return None


class DashboardClient:
    """
    Client class for the dashboard specific case
    """
    def __init__(self):
        self.client_id = 'dashboard'

    grant_types = [
        "implicit"
    ]

    def get_client_id(self):
        return self.client_id

    def check_client_type(self, client_type):
        return client_type == 'public'

    def check_client_secret(self, client_secret):
        raise NotImplementedError("Forbidden")

    def check_grant_type(self, grant_type):
        return grant_type == 'implicit'

    def check_redirect_uri(self, redirect_uri):
        return True

    def check_requested_scopes(self, scopes):
        # do not check scopes
        return True

    def check_response_type(self, response_type):
        return True

    def check_token_endpoint_auth_method(self, method):
        return method == 'none'

    def get_default_redirect_uri(self):
        return None

    def has_client_secret(self):
        return False


class Client:

    """
    The client Class is an Oauth adapter around the InstalledApplication model, that allows applying Authlib requirements
    on an installed application object
    """
    def __init__(self, app_service: ApplicationService, i_app: InstalledApplication):
        self.installed_app = i_app
        self.application_service = app_service
        self.client_id = i_app.bundle

    grant_types = [
        "authorization_code",
        "implicit"
    ]

    def get_client_id(self):
        return self.installed_app.bundle

    def check_client_type(self, client_type):
        # Both public and confidential client are allowed
        return True

    def check_client_secret(self, client_secret):
        return self.application_service.authenticate_application(self.installed_app.bundle,
                                                                 client_secret)

    def check_grant_type(self, grant_type):
        grant_types = self.installed_app.grant_types
        all_grants = grant_types.split(",")

        return grant_type in all_grants

    def check_redirect_uri(self, redirect_uri):
        if config.OAUTH2_ALWAYS_ACCEPT_REDIRECT_URI:
            return True

        url = urlparse(request.base_url)

        base_url = url.scheme + "://" + url.hostname + ":"

        for c in self.installed_app.installed_containers:
            if c.host_port:
                url_to_check = base_url + str(c.host_port)

                if redirect_uri.startswith(url_to_check):
                    return True

        return False

    def check_requested_scopes(self, scopes):
        # do not check scopes
        return True

    def check_response_type(self, response_type):
        return True

    def check_token_endpoint_auth_method(self, method):
        return method == self.installed_app.token_endpoint_auth_method

    def get_default_redirect_uri(self):
        return None

    def has_client_secret(self):
        return True


class BaseAuthCodeGrant:
    TOKEN_ENDPOINT_AUTH_METHODS = ['client_secret_basic', 'client_secret_post']
    """
    Create an authorization code
    :param client: the client that requesting the token.
    :param request: OAuth2Request instance.
    :return: code string

    """
    @inject
    def __init__(self, request, server):
        super().__init__(request,server)
        self.request = request
        self.server = server
        self.user_service = server.user_service

    def create_authorization_code(self, client, grant_user, request):

        nonce = request.data.get('nonce')

        return self.user_service.create_authorization_code(nonce=nonce, client_id=client.get_client_id(),
                                                           redirect_uri=request.redirect_uri, scope=request.scope,
                                                           user=grant_user)

    def parse_authorization_code(self, code, client):
        """
        Find an authorization code object

        :param code: a string represent the code.
        :param client: client related to this code.
        :return: authorization_code object

        """
        return self.user_service.authorization_code_by_code_and_client_id(code, client.get_client_id())

    def delete_authorization_code(self, authorization_code):
        """Delete authorization code from database or cache.

        :param authorization_code: the instance of authorization_code
        """
        self.user_service.delete_authorization_code(authorization_code)

    def authenticate_user(self, authorization_code):
        """Authenticate the user related to this authorization_code. Developers
        should implement this method in subclass, e.g.::

            def authenticate_user(self, authorization_code):
                return User.query.get(authorization_code.user_id)

        :param authorization_code: AuthorizationCode object
        :return: user
        """
        return self.user_service.find_user_by_user_id(authorization_code.user_id)


class OpenIDCodeGrant(BaseAuthCodeGrant, grants.OpenIDCodeGrant):
    pass


class OpenIDHybridGrant(BaseAuthCodeGrant, grants.OpenIDHybridGrant):
   pass


class OpenIDImplicitGrant(grants.OpenIDImplicitGrant):
   pass


class OIDCServerModule(Module):

    def __init__(self):
        self.app = None
        self.config = None
        self.user_service = None
        self.application_service = None

    def query_client(self, client_id):
        if client_id == 'dashboard':
            return DashboardClient()
        else:

            installed_application = self.application_service.get_installed_by_bundle(client_id)
            return Client(self.application_service, installed_application)

    def check_nonce(self, nonce, request):
        return False

    def save_token(self, token, request):
        user = current_user(self.user_service)

        if user:
            user_id = user.id
        elif request.user:
            user_id = request.user.id
        else:
            # client_credentials grant_type
            user_id = request.client.user_id
            # or, depending on how you treat client_credentials
            # user_id = None

        self.user_service.create_token(client_id=request.client.client_id,
                                       user_id=user_id,
                                       **token)

    @provider
    @singleton
    def provides_authorization_server(self, user_service: UserService,
                                      application_service: ApplicationService, app: App) -> AuthorizationServer:

        self.config = config
        self.user_service = user_service
        self.application_service = application_service
        self.app = app

        if self.config.AUTHLIB_INSECURE_TRANSPORT:
            os.environ['AUTHLIB_INSECURE_TRANSPORT']='true'
            os.putenv('AUTHLIB_INSECURE_TRANSPORT', 'true')

        bound_query_client = self.query_client.__get__(self, OIDCServerModule)
        bound_save_token = self.save_token.__get__(self, OIDCServerModule)
        bound_check_nonce = self.check_nonce.__get__(self, OIDCServerModule)

        server = AuthorizationServer(
            self.app, query_client=bound_query_client, save_token=bound_save_token
        )

        # associate the user repostiory to the server so that grants may have access to it
        server.user_service = user_service
        server.register_grant(OpenIDCodeGrant)
        server.register_grant(OpenIDHybridGrant)
        server.register_grant(OpenIDImplicitGrant)

        server.register_hook('exists_nonce', bound_check_nonce )

        return server

    @provider
    @singleton
    def provides_resource_protector(self, user_service:UserService) -> ResourceProtector:
        validator = OidcBearerTokenValidator(user_service)
        ResourceProtector.register_token_validator(validator)
        return ResourceProtector()


def use_oauth(app: App):
    @app.route('/oauth/authorize', methods=['GET', 'POST'])
    @inject
    def authorize(server: AuthorizationServer, user_service: UserService, application_service: ApplicationService):

        parsed_url = urlparse(request.base_url)

        scheme, hostname = parsed_url.scheme, get_hostname(parsed_url.netloc)

        # Login is required since we need to know the current resource owner.
        # It can be done with a redirection to the login page, or a login
        # form on this authorization page.
        cur_usr = current_user(user_service)

        if cur_usr is None:
            if request.method == 'GET':
                providers = application_service.auth_source_installed_application()

                return render_template(
                    'login.html',
                    providers=providers
                )
            else:
                cancel = request.form['cancel'] if 'cancel' in request.form else None

                if cancel:
                    return server.create_authorization_response(None)

                username = request.form['username']
                password = request.form['password']
                provider = None

                if 'provider' in request.form:
                    provider = request.form['provider']

                user = user_service.authenticate(username, password, provider, scheme, hostname)
                if user:
                    session['username'] = user.username
                    session['provider'] = provider
                    cur_usr = user
                else:
                    providers = application_service.auth_source_installed_application()
                    return render_template(
                        'login.html',
                        message="Login failed",
                        providers=providers
                    )
        else:
            return server.create_authorization_response(grant_user=cur_usr)

        if 'confirm' in request.form:
            # granted by resource owner
            return server.create_authorization_response(grant_user=cur_usr)
        # denied by resource owner
        return server.create_authorization_response(None)

    @app.route('/oauth/token', methods=['POST'])
    @inject
    def issue_token(server: AuthorizationServer):
        return server.create_token_response()
