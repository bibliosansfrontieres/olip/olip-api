from urllib.parse import urlparse

from Crypto.PublicKey import RSA
from Crypto.Util.number import long_to_bytes
from flask import session, request
from flask_restful import Resource
from flask_restplus import fields
from flask_restplus import Namespace
from injector import inject

import base64

from werkzeug.utils import redirect

from app.core.keys import Configuration
from app.tools.data_conversion import user_to_full_url
from app.tools.request_tools import protected_resource
from app.users import UserService
from authlib.flask.oauth2 import ResourceProtector

from app.users.models import Token

authorizations = {
    'Bearer': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

oauth_api = Namespace('oauth', description="API exposing oauth functions", authorizations=authorizations)

openid_configuration_model = oauth_api.model('OpenIdConfiguration', {
    'issuer': fields.String(description="OIDC authorization server identifier"),
    'authorization_endpoint': fields.String(description="URL of the authorize method"),
    'token_endpoint': fields.String(description="URL of the token redeem method"),
    'userinfo_endpoint': fields.String(description="URL of the user info method"),
    'jwks_uri': fields.String(description="URL pointing to the keystore used to validated signatures"),
    'scopes_supported': fields.List(fields.String(enum=["openid", "profile"]), description="Supported Oauth2 scopes"),
    'response_types_supported': fields.List(fields.String(enum=["code", "code id_token", "id_token", "token id_token"],
                                                          description="Supported OAuth response types")),
    'grant_types_supported': fields.List(fields.String(enum=["authorization_code", "implicit"],
                                                       description="Supported OAuth grant types")),
    'subject_types_supported': fields.List(fields.String(enum=["public"],
                                                         description="supported OAuth subject types")), # public
    'id_token_signing_alg_values_supported': fields.List(fields.String(enum=["RS256"], description="""Supported
            signature algorithms for the token redeem endpoint""")),
    'userinfo_signing_alg_values_supported': fields.List(fields.String(enum=["none"], description="""Supported
            signature algorithm for the user info endpoint""")),
    'end_session_endpoint': fields.String(description="URL for the log out")
})

jwk_model = oauth_api.model("Jwk", {
    'kty': fields.String(enum=["RSA"], description="Key type"),
    'n': fields.String(description="Modulus of the key"),
    'e': fields.String(description="Public exponent of the key"),
    'alg': fields.String(enum=["RS256"], description="Algorithm of the key"),
    'kid': fields.String(description="Id of the key")
})

jwks_model = oauth_api.model("Jwks", {
    'keys': fields.List(fields.Nested(jwk_model), description="List of keys")
})


@oauth_api.route(".well-known/openid-configuration")
class OpenIdConfigurationResource(Resource):

    @inject
    def __init__(self, api):
        self.api = api

    @oauth_api.marshal_with(openid_configuration_model)
    def get(self):
        """
        Gets the openid connect configuration
        """
        base_url = self.api.apis[0].base_url

        response = {
            'issuer': base_url,
            'authorization_endpoint': base_url + "oauth/authorize",
            'token_endpoint': base_url+ "oauth/token",
            'userinfo_endpoint': base_url + "oauth/userinfo",
            'jwks_uri': base_url + "oauth/jwks",
            'scopes_supported': ['openid'],
            'response_types_supported': ["code", "code id_token", "id_token", "token id_token"],
            'grant_types_supported': ["authorization_code", "implicit"],
            'subject_types_supported': ['public'],
            'id_token_signing_alg_values_supported': ['RS256'],
            'userinfo_signing_alg_values_supported': ['none'],
            'end_session_endpoint': base_url + "oauth/logout"
        }

        return response


@oauth_api.route("oauth/jwks")
class OpenIdJwk(Resource):

    @inject
    def __init__(self, config: Configuration, api):
        self.config = config
        self.api = api

    @oauth_api.marshal_with(jwks_model)
    def get(self):
        """
        Gets the keystore that can be used to verify signatures
        """
        pub_key = RSA.importKey(self.config.OAUTH2_JWT_PUBLIC_KEY)

        return { 'keys': [
            {
                'kty': 'RSA',
                'n': base64.b64encode(long_to_bytes(pub_key.n)).decode('ascii'),
                'e': base64.b64encode(long_to_bytes(pub_key.e)).decode('ascii'),
                'alg': 'RS256',
                'kid': 'None'  # key from config.py is a string, not a dict
            }
        ]}


user_info_model = oauth_api.model("UserInfo", {
    'sub': fields.String(description="User subject (i.e. provider plus username)"),
    'provider': fields.String(description="Provider in case of external authentication"),
    'username': fields.String(description="Username"),
    'url': fields.String(description="Url to get user detail from the API"),
    'name': fields.String(description="Full name of the user"),
    'roles': fields.List(fields.String(description="Roles of the user"))
})


@oauth_api.route("oauth/userinfo")
class UserInfoResource(Resource):

    @inject
    def __init__(self, user_service: UserService, api, resource_protector: ResourceProtector):
        self.user_service = user_service
        self.api = api
        self.resource_protector = resource_protector

    @oauth_api.marshal_with(user_info_model, skip_none=True)
    @oauth_api.doc(security='Bearer')
    @protected_resource()
    def get(self):
        """
        Gets the user info
        """
        token = self.resource_protector.acquire_token('openid')

        user = self.user_service.find_user_by_user_id(token.user_id)

        results = {
            'sub': user.get_user_id(),
            'username': user.username,
            'url': user_to_full_url(oauth_api, user)
        }

        if user.admin:
            results['roles']=['admin']

        if user.provider:
            results['provider'] = user.provider

        if 'profile' in token.scope:
            results['name'] = user.name

        return results


@oauth_api.route("oauth/logout")
class LogoutResource(Resource):

    @inject
    def __init__(self, config:Configuration, api):
        self.api = api
        self.config = config

    def get(self):
        """
        Logoff the current user
        """
        session.clear()

        url = self.config.OAUTH2_LOGOUT_REDIRECT_URI

        if not url:
            hostname = urlparse(self.config.API_BASE_URL).hostname
            url = "http://"+hostname

        return redirect(url, code=302)
