import os
from injector import inject
from sqlalchemy.orm.exc import NoResultFound
from flask import request
from app.applications.repository import ApplicationRepository
from app.logo.models import (Logo)
from app.logo.repository import LogoRepository
from app.tools import data_conversion
from app.tools.errors import ForbiddenError, NotFoundError


class LogoService:

    # service managing categories, playlist and terms
    @inject
    def __init__(self, logo_repository: LogoRepository):
        self.logo_repository = logo_repository

    def get_logo_thumbnail(self):
        """
        Get the thumbnail for admin. If no thumbnail has been defined previously, returns
        a default image (img\admn.png)
        :return: a tuple (Content Type, binary data) if the thumbnail is stored in the database, or a tuple
        (Content Type, string) whose second member denote a file path
        """
        try:
            logo = self.logo_repository.get_logo(id=1)
            return logo.thumbnail_mime_type, logo.thumbnail
        except NoResultFound:
            raise NotFoundError("logo not found")

    def set_logo_thumbnail(self, content_type, data):
        """
        Defines the Admin thumbnail
        :param content_type: Content type of the picture sent
        :param data: Binary array containing the picture
        """
        try:
            logo = self.logo_repository.get_logo(id=1)

            if not logo:
                logo = Logo(thumbnail=data, thumbnail_mime_type=content_type)
                self.logo_repository.save(logo)
            else:
                logo.thumbnail_mime_type = content_type
                logo.thumbnail = data
        except NoResultFound:
            raise NotFoundError("logo not found")
