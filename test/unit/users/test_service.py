import config
import time

from unittest.mock import Mock

from app.applications import ApplicationRepository
from app.applications.models import InstalledApplication, ApplicationState, InstalledContainer
from app.core.http_client import HttpClient
from app.platform import PlatformService
from app.users import UserRepository, UserService
from app.users.models import User, AuthorizationCode, Token
from app.core.password_hasher import PasswordHasher
from test.unit.abstract_unit_test import AbstractUnitTest


class UserServiceTest(AbstractUnitTest):

    def setUp(self):
        self.password_hasher = Mock(spec=PasswordHasher)
        self.user_repository_mock = Mock(spec=UserRepository)
        self.platform_service_mock = Mock(spec=PlatformService)
        self.http_client_mock = Mock(spec=HttpClient)
        self.application_repository_mock = Mock(spec=ApplicationRepository)

        self.user_service = UserService(self.user_repository_mock, self.password_hasher, config,
                                        self.platform_service_mock, self.http_client_mock,
                                        self.application_repository_mock)

    def test_get_all(self):
        # when
        self.user_service.get_all()

        # then
        self.user_repository_mock.get_all.assert_called_once_with()

    def test_get_by_username_and_provider(self):
        # given
        user = User(username="john.doe", name="John Doe", password="===123123===", provider="test.app")
        self.user_repository_mock.find_user_by_username_and_provider.return_value = user

        # when
        res = self.user_service.get_by_username_and_provider("john.doe", provider="test.app")

        # then
        self.assertEqual(user, res)
        self.user_repository_mock.find_user_by_username_and_provider.assert_called_once_with("john.doe", "test.app")

    def test_create_user(self):
        # given
        self.password_hasher.hash.return_value = "hashed_password"
        self.user_repository_mock.find_user_by_username_and_provider.return_value = None

        # when
        response = self.user_service.create_or_update_user("john.doe", "John Doe", admin=True, password="123")

        # then
        user = User(username="john.doe", name="John Doe", admin=True, password="hashed_password")
        self.password_hasher.hash.assert_called_once_with("123")
        self.user_repository_mock.save.assert_called_once_with(user)
        self.assertEqual(response, user)

    def test_create_external_user(self):
        # given
        self.password_hasher.hash.return_value = "hashed_password"
        self.user_repository_mock.find_user_by_username_and_provider.return_value = None

        # when
        response = self.user_service.create_or_update_user("john.doe", "John Doe", provider="test.app")

        # then
        user = User(username="john.doe", name="John Doe", provider="test.app")
        self.password_hasher.hash.assert_not_called()
        self.user_repository_mock.save.assert_called_once_with(user)
        self.assertEqual(response, user)

    def test_update_user(self):
        # given
        user = User(username="john.doe", name="John", admin=False, password="456")
        self.user_repository_mock.find_user_by_username_and_provider.return_value = user

        self.password_hasher.hash.return_value = "hashed_password"

        # when
        response = self.user_service.create_or_update_user("john.doe", "John Doe", admin=True, password="123")

        # then
        self.user_repository_mock.find_user_by_username_and_provider.assert_called_once_with("john.doe", provider=None)
        self.password_hasher.hash.assert_called_once_with("123")
        expected_user = User(username="john.doe", name="John Doe", admin=True, password="hashed_password")
        self.assertEqual(expected_user, user)
        self.assertEqual(response, user)

    def test_update_without_password(self):
        """
        Ensure that updating a user without specifying a password doesn't update the password
        """
        # given
        user = User(username="john.doe", name="John", password="456")
        self.user_repository_mock.find_user_by_username_and_provider.return_value = user

        # when
        self.user_service.create_or_update_user("john.doe", "John Doe", password=None)

        # then
        self.user_repository_mock.find_user_by_username_and_provider.assert_called_once_with("john.doe", provider=None)
        self.assertEqual("456", user.password)

    def test_update_external_user(self):
        # given
        user = User(username="john.doe", name="John", provider="test.app")
        self.user_repository_mock.find_user_by_username_and_provider.return_value = user

        # when
        response = self.user_service.create_or_update_user("john.doe", "John Doe", provider="test.app")

        # then
        self.user_repository_mock.find_user_by_username_and_provider.assert_called_once_with("john.doe", provider="test.app")
        self.password_hasher.hash.assert_not_called()
        expected_user = User(username="john.doe", name="John Doe", provider="test.app")
        self.assertEqual(expected_user, user)
        self.assertEqual(response, user)

    def test_create_or_update_user_with_both_password_and_provider(self):
        """
        Ensure that one can not create a user with both an external provider and a password
        """
        with self.assertRaises(ValueError):
          self.user_service.create_or_update_user("john.doe", "John Doe", password="123", provider="test.app")

    def test_authenticate_with_good_password(self):
        # given
        user = User(username="john.doe", name="John", password="hashed_password")
        self.user_repository_mock.find_user_by_username_and_provider.return_value = user
        self.password_hasher.verify.return_value = True

        # when
        response = self.user_service.authenticate("john.doe", "Clear text password")

        # then
        self.user_repository_mock.find_user_by_username_and_provider.assert_called_once_with("john.doe")
        self.password_hasher.verify.assert_called_once_with("Clear text password", "hashed_password")
        self.assertEqual(user, response)

    def test_authenticate_with_wrong_password(self):
        # given
        user = User(username="john.doe", name="John", password="hashed_password")
        self.user_repository_mock.find_user_by_username_and_provider.return_value = user
        self.password_hasher.verify.return_value = False

        # when
        response = self.user_service.authenticate("john.doe", "Clear text password")

        # then
        self.assertIsNone(response)

    def test_authenticate_with_inexisting_user(self):
        # given
        self.user_repository_mock.find_user_by_username_and_provider.return_value = None
        self.password_hasher.verify.return_value = False

        # when
        response = self.user_service.authenticate("john.doe", "Clear text password")

        # then
        self.assertIsNone(response)

    def test_authenticate_with_external_provider(self):
        """
        Ensure that an authentication with an external provider leads to the call of the external provider
        and the creation of the user if he doesn't exist
        """
        # given
        provider = InstalledApplication(bundle="test.app",
                                        current_state=ApplicationState.installed,
                                        target_version="1.0.0",
                                        auth_source_secret="token",
                                        auth_source_container="cauth",
                                        auth_source_url="/auth/url",
                                        search_container="c1")

        InstalledContainer("test.app.cauth", provider, "ipfs:test/bsf", "test/bsf", 8383)

        self.application_repository_mock.find_installed_application_by_bundle.return_value = provider

        self.http_client_mock.get.return_value = 200, {
            "verified": True,
            "profile": {
                "name": "John Doe"
            }
        }
        self.user_repository_mock.find_user_by_username_and_provider.return_value = None

        # when
        response = self.user_service.authenticate("john.doe", "password",
                                                  provider="test.app",
                                                  host="localhost",
                                                  scheme="http")

        # then
        expected_user = User(username="john.doe", name="John Doe", password=None, provider="test.app")
        self.assertEqual(expected_user, response)

        if config.PROXY_ENABLE:
            self.http_client_mock.get.assert_called_with("http://c1.localhost/auth/url",
                                                        auth=("john.doe", "password"),
                                                        headers={"X-Auth-Token": "token"})
        else:
            self.http_client_mock.get.assert_called_with("http://localhost:8383/auth/url",
                                                        auth=("john.doe", "password"),
                                                        headers={"X-Auth-Token": "token"})

        self.user_repository_mock.save.assert_called_once_with(expected_user)

    if not config.PROXY_ENABLE:
        def test_authenticate_with_external_provider_when_internal_host_base_url_is_set(self):
            """
            Ensure that the call to the external authenticate endpoint uses the configuration
            parameter INTERNAL_HOST_BASE_URL if defined
            """
            # given
            config.INTERNAL_HOST_BASE_URL = "http://1.2.3.4"

            provider = InstalledApplication(bundle="test.app",
                                            current_state=ApplicationState.installed,
                                            target_version="1.0.0",
                                            auth_source_secret="token",
                                            auth_source_container="cauth",
                                            auth_source_url="/auth/url")

            InstalledContainer("test.app.cauth", provider, "ipfs:test/bsf", "test/bsf", 8383)

            self.application_repository_mock.find_installed_application_by_bundle.return_value = provider

            # when
            self.user_service.authenticate("john.doe", "password",
                                                      provider="test.app",
                                                      host="localhost",
                                                      scheme="http")

            # then
            self.http_client_mock.get.assert_called_with("http://1.2.3.4:8383/auth/url",
                                                        auth=("john.doe", "password"),
                                                        headers={"X-Auth-Token": "token"})

    def test_authentication_failure_with_external_provider(self):
        # given
        provider = InstalledApplication(bundle="test.app",
                                        current_state=ApplicationState.installed,
                                        target_version="1.0.0",
                                        auth_source_container="cauth",
                                        auth_source_url="/auth/url")

        InstalledContainer("test.app.cauth", provider, "test/bsf", 8383)

        self.application_repository_mock.find_installed_application_by_bundle.return_value = provider
        self.http_client_mock.get.return_value = 200, {
            "verified": False,
            "profile": {
                "name": "John Doe"
            }
        }

        # when
        response = self.user_service.authenticate("john.doe", "password",
                                                  provider="test.app",
                                                  host="localhost",
                                                  scheme="http")

        # then
        self.assertEqual(None, response)

    def test_authentication_failure_bad_status_with_external_provider(self):
        # given
        provider = InstalledApplication(bundle="test.app",
                                        current_state=ApplicationState.installed,
                                        target_version="1.0.0",
                                        auth_source_container="cauth",
                                        auth_source_url="/auth/url")

        InstalledContainer("test.app.cauth", provider, "test/bsf", 8383)

        self.application_repository_mock.find_installed_application_by_bundle.return_value = provider
        self.http_client_mock.get.return_value = 403, "Authentication error"

        # when
        response = self.user_service.authenticate("john.doe", "password",
                                                  provider="test.app",
                                                  host="localhost",
                                                  scheme="http")

        # then
        self.assertEqual(None, response)

    def test_authentication_failure_bad_json_content(self):
        # given
        provider = InstalledApplication(bundle="test.app",
                                        current_state=ApplicationState.installed,
                                        target_version="1.0.0",
                                        auth_source_container="cauth",
                                        auth_source_url="/auth/url")

        InstalledContainer("test.app.cauth", provider, "test/bsf", 8383)

        self.application_repository_mock.find_installed_application_by_bundle.return_value = provider

        self.http_client_mock.get.side_effect = ValueError ("JSON Decoding error")

        # when
        response = self.user_service.authenticate("john.doe", "password",
                                                  provider="test.app",
                                                  host="localhost",
                                                  scheme="http")

        # then
        self.assertEqual(None, response)

    def test_authorization_code_by_code_and_client_id(self):
        # given
        auth_code = AuthorizationCode()

        self.user_repository_mock.authorization_code_by_code_and_client_id.return_value = auth_code

        # when
        response = self.user_service.authorization_code_by_code_and_client_id("ABCDE", "test.app")

        # then
        self.assertEqual(auth_code, response)
        self.user_repository_mock.authorization_code_by_code_and_client_id.assert_called_once_with("ABCDE", "test.app")

    def test_delete_authorization_code(self):
        # given
        auth_code = AuthorizationCode()

        # when
        self.user_service.delete_authorization_code(auth_code)

        # then
        self.user_repository_mock.delete.assert_called_once_with(auth_code)

    def test_find_user_by_user_id(self):
        # given
        user = User("jdoe", "John Doe", "ABCD")

        # when
        self.user_service.find_user_by_user_id("jdoe")

        # then
        self.user_repository_mock.find_user_by_user_id.assert_called_once_with("jdoe")

    def test_create_token(self):
        # when
        self.user_service.create_token("client.id","user.id", token_type="access")

        # then
        token = Token(client_id = "client.id", user_id="user.id", token_type="access")

        self.user_repository_mock.save.assert_called_once_with(token)

    def test_create_admin_user_if_no_user_exists(self):
        # given
        self.user_repository_mock.count_users.return_value = 0
        self.user_repository_mock.find_user_by_username_and_provider.return_value = None

        self.password_hasher.hash.return_value = "hashed_password"

        # when
        admin_created = self.user_service.create_admin_user_if_no_user_exists()

        # then
        self.assertTrue(admin_created)
        self.password_hasher.hash.assert_called_once_with("admin")
        self.user_repository_mock.save.assert_called_once_with(
            User(username="admin", name="Admin", admin=True, password="hashed_password")
        )

    def test_create_admin_if_user_exists(self):
        # given
        self.user_repository_mock.count_users.return_value = 1
        self.user_repository_mock.find_user_by_username_and_provider.return_value = None

        # when
        admin_created = self.user_service.create_admin_user_if_no_user_exists()

        # then
        self.assertFalse(admin_created)
        self.user_repository_mock.save.assert_not_called()

    def test_validate_token(self):
        # given
        t = time.time()*1000
        token = Token(
            123, "admin", "client_id", issued_at=t, expires_in=1800
        )
        self.user_repository_mock.find_token_by_access_token.return_value = token
        self.platform_service_mock.current_time.return_value = t+1799*60*1000

        # when
        returned_token = self.user_service.validate_token(123)

        # then
        self.user_repository_mock.find_token_by_access_token.assert_called_once_with(123)
        self.assertEqual(token, returned_token)

    def test_token_expiry(self):
        # given
        t = time.time()*1000
        token = Token(
            123, "admin", "client_id", issued_at=t, expires_in=1800
        )
        self.user_repository_mock.find_token_by_access_token.return_value = token
        self.platform_service_mock.current_time.return_value = t+1801*60*1000

        # when
        returned_token = self.user_service.validate_token(123)

        # then
        self.assertIsNone(returned_token)

    def test_token_is_none(self):
        # given
        self.user_repository_mock.find_token_by_access_token.return_value = None

        # when
        returned_token = self.user_service.validate_token(123)

        # then
        self.assertIsNone(returned_token)
