from unittest.mock import Mock
from lxml import etree
from flask_injector import FlaskInjector
from flask_restplus import Api
from injector import provider, singleton, Module
from xmlunittest import XmlTestCase
from flask import Flask
from app.search import SearchModule
from app.search.models import SearchResult
from app.search.resources import search_api
from app.search.service import SearchService

import config

class ServiceMockModule(Module):
    def __init__(self, mock):
        self.mock = mock

    @provider
    @singleton
    def provide_mock(self) -> SearchService:
        return self.mock


class SearchResourceTestCase(XmlTestCase):

    def setUp(self):
        app = Flask(__name__, instance_relative_config=True)
        # Load the config file
        config.DISABLE_AUTH = True
        app.config.from_object(config)

        # Initiate the Flask API
        api = Api(
            title='Box API',
            version='1.0',
            description='Box API',
            # All API metadatas
        )

        # Load application namespace
        api.add_namespace(search_api, path="/opensearch")

        # Bootstrap app
        api.init_app(app)

        self.service_mock=Mock(spec=SearchService)
        modules = [SearchModule, ServiceMockModule(self.service_mock)]
        self.injector = FlaskInjector(app=app, modules=modules)

        self.client = app.test_client()

    def test_search(self):
        # when
        self.service_mock.query_apps.return_value = [
            SearchResult(title="result1", url="http://localhost/result1", description="description1"),
            SearchResult(title="result2", url="http://localhost/result2", description="description2")
        ]

        # when
        response = self.client.get("/opensearch/search?q=searchedTerm")

        # then
        self.service_mock.query_apps.assert_called_once_with("http", "localhost", "searchedTerm")
        root = etree.fromstring(response.data)

        self.assertEqual('Olip search results', self._atom_xpath(root, './x:title')[0].text)

        self.assertEqual(1, len(self._atom_xpath(root, "./link[@href='http://localhost/opensearch/search?q=searchedTerm']")));

    def _atom_xpath(self, root, xpath):
        return root.xpath('./x:title', namespaces={
            'x': 'http://www.w3.org/2005/Atom'
        })

