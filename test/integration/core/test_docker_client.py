import time
import docker
from unittest import TestCase

from docker.errors import NotFound

import config
from app.core.docker_client import DockerClient
import socket


class DockerClientTest(TestCase):

    @staticmethod
    def wait_for_status(container, expected_status):
        counter = 0
        while container.status != expected_status and counter < 5:
            time.sleep(1)
            container.reload()
            counter += 1

        return container.status

    @staticmethod
    def try_remove_container(container):
        try:
            container.stop()
            counter = 0
            while container.status != 'exited' and counter < 5:
                time.sleep(1)
                container.reload()
                counter += 1
        except Exception as e:
            print(e)

        try:
            container.remove()
        except Exception as e:
            print(e)

    def setUp(self):

        self.docker_client = DockerClient()
        self.docker_sdk = docker.from_env()

    def tearDown(self):
        try:
            self.docker_sdk.images.remove("localhost:5000/ciqnxutnaxgi37fpzr2u2gs2l6gbwegxj7a63tu77gw4mwq6hirwkyq/busybox:latest")
        except:
            pass

        self.docker_sdk.close()

    def _assertConnectionSucceed(self, port, host):

        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, port))
            s.close()
        except ConnectionRefusedError as e:
            self.fail("Connection on {} port {} has been refused".format(host, port))
