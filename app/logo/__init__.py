from injector import singleton, Module

from app.logo.repository import LogoRepository
from app.logo.service import LogoService


class LogoModule(Module):

    def configure(self, binder):
        binder.bind(LogoService, to=LogoService, scope=singleton)
        binder.bind(LogoRepository, to=LogoRepository, scope=singleton)
