from threading import Thread
from flask_sqlalchemy import SQLAlchemy

import schedule
import time

from injector import inject, Module, singleton, provider
from app.applications import StateMachine, ApplicationService

import logging

logger = logging.getLogger('scheduler')


class Scheduler(Thread):

    @inject
    def __init__(self, application_service: ApplicationService, state_machine: StateMachine, db: SQLAlchemy):
        super().__init__()
        self.application_service = application_service
        self.state_machine = state_machine
        self.db = db

    def run(self):
        logger.info("Starting scheduler %s", self)

        schedule.every(24).hours.do(self.refresh_repository)
        schedule.every(1).minutes.do(self.ping_state_machine)

        while True:
            schedule.run_pending()
            time.sleep(5)

    def refresh_repository(self):
        try:
            logger.info("Starting repository update")
            self.application_service.update_repository()
            self.db.session.commit()
            logger.info("End of repository update")
        except Exception as e:
            print(e)
            self.db.session.rollback()
            raise

    def ping_state_machine(self):
        self.state_machine.ping()


class SchedulerModule(Module):

    @provider
    @singleton
    def provide_scheduler(self,
                          application_service: ApplicationService,
                          state_machine: StateMachine,
                          db: SQLAlchemy) -> Scheduler:
        sched = Scheduler(application_service, state_machine, db)

        sched.start()

        return sched

