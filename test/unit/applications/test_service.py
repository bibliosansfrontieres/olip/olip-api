from unittest.mock import Mock, call

from app.applications.models import Application, Container, InstalledApplication, ApplicationState, InstalledContainer, \
    Content, InstalledContent, ContentState, EndpointType, ConfigParameter, ConfigurationValue, ContainerConfig, \
    Configuration
from app.applications.service import ApplicationService, Endpoint, NotFoundError
from app.applications.repository import ApplicationRepository
from app.applications.state_machine.state_machine import StateMachine
from app.applications.storage.storage import Storage
from app.applications.storage.storage_manager import StorageManager
from app.categories.models import Category, Playlist
from app.categories.service import CategoryService
from app.core.password_hasher import PasswordHasher
from app.users.models import User
from test.unit.abstract_unit_test import AbstractUnitTest
import config


class ApplicationServiceTest(AbstractUnitTest):
    """ Test of the application service"""

    def setUp(self):
        self.application_repository_mock = Mock(spec=ApplicationRepository)
        self.state_machine_mock = Mock(spec=StateMachine)
        self.password_hasher_mock = Mock(spec=PasswordHasher)
        self.category_service_mock = Mock(spec=CategoryService)

        self.storage_manager_mock = Mock(spec=StorageManager)
        self.storage_mock = Mock(spec=Storage)
        self.storage_manager_mock.storage_for_url.return_value = self.storage_mock

        self.application_service = ApplicationService(self.application_repository_mock, self.storage_manager_mock,
                                                      self.state_machine_mock, config, self.password_hasher_mock,
                                                      self.category_service_mock)

    def test_get_all(self):
        """
        Test the correct return of application and installed applications by the get_all function
        """
        app = Application(name="test", bundle="test.app", version="1.0.0")
        app_alone = Application(name="test", bundle="test.app.alone", version="1.0.0")
        installed = InstalledApplication(bundle="test.app", current_state=ApplicationState.uninstalled,
                                         target_state=ApplicationState.downloaded, target_version="1.0.0")
        installed_alone = InstalledApplication(bundle="test.app.inst.alone", current_state=ApplicationState.uninstalled,
                                               target_state=ApplicationState.downloaded, target_version="1.0.0")
        self.application_repository_mock.get_all.return_value = [ app, app_alone ]
        self.application_repository_mock.get_all_installed.return_value = [ installed, installed_alone ]

        lst = self.application_service.get_all()

        self.assertEqual(3, len(lst))
        self.assertTrue( (app, installed) in lst)
        self.assertTrue( (None, installed_alone) in lst)
        self.assertTrue( (app_alone, None) in lst)

    def test_get_all_order(self):
        # given

        app1 = Application(name="test", bundle="test.app.1", version="1.0.0")
        app2 = Application(name="test", bundle="test.app.2", version="1.0.0")
        i_app1 = InstalledApplication(bundle="test.app.1", current_state=ApplicationState.installed,
                                      target_state=ApplicationState.installed, target_version="1.0.0",
                                      display_weight=2)
        i_app2 = InstalledApplication(bundle="test.app.2", current_state=ApplicationState.installed,
                                      target_state=ApplicationState.installed, target_version="1.0.0",
                                      display_weight=1)

        # put app and installed app in reverse order to ensure order is consistent
        self.application_repository_mock.get_all.return_value = [ app1, app2 ]
        self.application_repository_mock.find_installed_application_by_current_state.return_value = [ i_app2, i_app1 ]

        # when
        lst = self.application_service.get_all(current_state=ApplicationState.installed)

        # then
        self.assertEqual(2, len(lst))
        print(lst)
        self.assertTrue("test.app.2", lst[0][1].bundle)
        self.assertTrue("test.app.1", lst[1][1].bundle)

    def test_get_all_with_current_state(self):
        app = Application(name="test", bundle="test.app", version="1.0.0")
        app_alone = Application(name="test", bundle="test.app.alone", version="1.0.0")
        installed = InstalledApplication(bundle="test.app", current_state=ApplicationState.downloaded,
                                         target_state=ApplicationState.downloaded, target_version="1.0.0")
        installed_alone = InstalledApplication(bundle="test.app.inst.alone",
                                               current_state=ApplicationState.downloaded,
                                               target_state=ApplicationState.downloaded,
                                               target_version="1.0.0")
        self.application_repository_mock.get_all.return_value = [app, app_alone]
        self.application_repository_mock.find_installed_application_by_current_state.return_value = [installed, installed_alone]

        lst = self.application_service.get_all(current_state=ApplicationState.downloaded)

        self.application_repository_mock.find_installed_application_by_current_state.assert_called_once_with(ApplicationState.downloaded)

        self.assertEqual(2, len(lst))
        self.assertTrue((app, installed) in lst)
        self.assertTrue((None, installed_alone) in lst)

    def test_get_all_with_visible(self):

        installed = InstalledApplication(bundle="test.app", current_state=ApplicationState.downloaded,
                                         target_state=ApplicationState.downloaded, target_version="1.0.0", visible=True)
        installed_not_vis = InstalledApplication(bundle="test.app.inst.alone",
                                               current_state=ApplicationState.downloaded,
                                               target_state=ApplicationState.downloaded,
                                               target_version="1.0.0",
                                               visible=False)
        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = [installed, installed_not_vis]

        lst = self.application_service.get_all(visible=False)

        self.application_repository_mock.get_all_installed.assert_called_once_with()

        self.assertEqual(1, len(lst))
        self.assertTrue((None, installed_not_vis) in lst)

    def test_get_all_with_repository_update(self):
        # given
        config.BOX_REPOSITORY_IPN="ipfs:/ipns/ANIPNPOINTER"
        self._setup_descriptor_with_json("sample_descriptor.json")

        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = []

        # when
        self.application_service.get_all(repository_update=True)

        # then
        self.storage_manager_mock.storage_for_url.assert_called_once_with("ipfs:/ipns/ANIPNPOINTER")
        self.storage_mock.download_descriptor.assert_called_once_with("ipfs:/ipns/ANIPNPOINTER")

        app1 = Application(name="Test App", bundle="test.app", description="Lorem ipsum", version="1.0.0", picture="==BASE64ENCODEDIMAGE==")
        app2 = Application(name="Test App 2", bundle="test.app.2", description="Lorem ipsum 2", version="2.0.0",
                           picture="==BASE64ENCODEDIMAGE2==", grant_types="authorization_code,implicit",
                           response_types="id_token,code id_token", token_endpoint_auth_method="client_secret_post")
        containers1 = [Container(image="anipfshash/busybox:latest", application=app1, name="c1"),
                       Container(image="asecondipfshash/busybox:latest", application=app1, name="c2")]
        containers2 = [Container(image="athirdipfshash/busybox:latest", application=app2, name="c3")]
        app1.containers = containers1
        app2.containers = containers2

        self.application_repository_mock.save.assert_has_calls([
            call(Container(image="anipfshash/busybox:latest", application=app1, name="c1")),
            call(Container(image="asecondipfshash/busybox:latest", application=app1, name="c2")),
            call(Content(name="afile", application=app1, content_id="test.content",
                         download_path="/ipfs/Qma6moV1boFTUp4xiEh3WRX8WEYpsKbZXWySnsS6HQ65iJ",
                         destination_path="path/to/file.pdf", description="desc", version="1.0.0")),
            call(app1),
            call(Container(image="athirdipfshash/busybox:latest", application=app2, name="c3", expose=80)),
            call(app2)
        ])

    def test_content_update_with_endoint(self):
        # given
        config.BOX_REPOSITORY_IPN = "/ipns/ANIPNPOINTER"

        self._setup_descriptor_with_json("descriptor_with_endpoint_in_content.json")

        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = []

        # when
        self.application_service.get_all(repository_update=True)

        # then
        app1 = Application(name="Test App", bundle="test.app", version="1.0.0")
        self.application_repository_mock.save.assert_any_call(
            Content(name="afile", application=app1, content_id="test.content",
                         download_path="/ipfs/Qma6moV1boFTUp4xiEh3WRX8WEYpsKbZXWySnsS6HQ65iJ",
                         destination_path="path/to/file.pdf", version="1.0.0",
                         endpoint_container="c1", endpoint_name="Endpoint name", endpoint_url="/url")
        )

    def test_repository_update_with_existing_app(self):
        # given
        app = Application(name="Test", bundle="test.app", version="1.0.0")
        self.application_repository_mock.find_application_by_bundle.return_value=app
        self.application_repository_mock.get_all.return_value=[]
        self.application_repository_mock.get_all_installed.return_value = []
        self._setup_descriptor_with_json({ 'applications': [ { 'name': 'Test', 'version': '2.0.0', 'bundle': "test.app", 'containers':[]} ]})

        # when
        self.application_service.get_all(repository_update=True)

        # then
        self.application_repository_mock.find_application_by_bundle.assert_called_once_with('test.app')
        self.application_repository_mock.delete.assert_called_once_with(app)
        self.application_repository_mock.flush.assert_called_once_with()
        self.application_repository_mock.save.assert_called_once_with(Application(name="Test", version="2.0.0", bundle="test.app"))

    def test_get_all_with_repository_update_with_content_to_update(self):
        # given
        app = Application(name="Test", bundle="test.app", version="1.0.0")
        installed_app = InstalledApplication(bundle="test.app", target_version="1.0.0", has_content_to_upgrade=True)
        InstalledContent("c1", "Content",
                                             installed_app, "down", "dest",
                                             ContentState.installed,
                                             ContentState.installed, "1.0.0", current_version="0.0.1")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_app
        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = []
        self._setup_descriptor_with_json({
            'applications': [{'name': 'Test', 'version': '2.0.0', 'bundle': "test.app", 'containers': [],
                              'contents':[{
                                'content_id': 'c1',
                                'name': 'Content',
                                'download_path': 'down',
                                'destination_path': 'dest',
                                'description': 'desc',
                                'version': '2.0.0'
                              }]}]})

        # when
        self.application_service.get_all(repository_update=True)

        # then
        self.assertTrue(installed_app.has_content_to_upgrade)

    def test_get_all_with_repository_update_with_content_to_update_with_uninstalled_app(self):
        # given
        app = Application(name="Test", bundle="test.app", version="1.0.0")
        installed_app = InstalledApplication(bundle="test.app", target_version="1.0.0", has_content_to_upgrade=False,
                                             current_state=ApplicationState.uninstalled)
        InstalledContent("c1", "Content",
                                             installed_app, "down", "dest",
                                             ContentState.installed,
                                             ContentState.installed, "1.0.0", current_version="0.0.1")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_app
        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = []
        self._setup_descriptor_with_json({
            'applications': [{'name': 'Test', 'version': '2.0.0', 'bundle': "test.app", 'containers': [],
                              'contents':[{
                                'content_id': 'c1',
                                'name': 'Content',
                                'download_path': 'down',
                                'destination_path': 'dest',
                                'description': 'desc',
                                'version': '2.0.0'
                              }]}]})

        # when
        self.application_service.get_all(repository_update=True)

        # then
        self.assertFalse(installed_app.has_content_to_upgrade)

    def test_get_all_with_repository_update_with_content_to_update_with_uninstalled_content(self):
        # given
        app = Application(name="Test", bundle="test.app", version="1.0.0")
        installed_app = InstalledApplication(bundle="test.app", target_version="1.0.0", has_content_to_upgrade=False,
                                             current_state=ApplicationState.installed)
        InstalledContent("c1", "Content", installed_app, "down", "dest",
                                             ContentState.uninstalled,
                                             ContentState.uninstalled, "1.0.0", current_version="0.0.1")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_app
        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = []
        self._setup_descriptor_with_json({
            'applications': [{'name': 'Test', 'version': '2.0.0', 'bundle': "test.app", 'containers': [],
                              'contents':[{
                                'content_id': 'c1',
                                'name': 'Content',
                                'download_path': 'down',
                                'destination_path': 'dest',
                                'description': 'desc',
                                'version': '2.0.0'
                              }]}]})

        # when
        self.application_service.get_all(repository_update=True)

        # then
        self.assertFalse(installed_app.has_content_to_upgrade)

    def test_get_all_with_repository_update_with_image_link(self):
        """
        Ensure that an image reference to the descriptor's storage is resolved via
        the download_descriptor_resource of the storage
        """
        # given
        config.BOX_REPOSITORY_IPN="ipfs:/ipns/ANIPNPOINTER"
        self._setup_descriptor_with_json({ 'applications': [
            { 'name': 'Test',
              'picture': '#image.png',
              'version': '2.0.0',
              'bundle': "test.app",
              'containers':[]
            }]})

        self.storage_mock.download_descriptor_resource.return_value = b"121212"

        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = []

        # when
        self.application_service.get_all(repository_update=True)

        # then
        self.storage_mock.download_descriptor_resource.assert_called_once_with("ipfs:/ipns/ANIPNPOINTER", "image.png")

        # MTIxMjEy = base64(121212)
        app1 = Application(name="Test", bundle="test.app", version="2.0.0", picture="MTIxMjEy")

        self.application_repository_mock.save.assert_has_calls([
            call(app1)
        ])

    def test_get_all_with_repository_update_with_search(self):
        # given
        self._setup_descriptor_with_json("descriptor_with_search.json")

        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = []

        # when
        self.application_service.get_all(repository_update=True)

        # then
        self.application_repository_mock.save.assert_has_calls([
                call(Application(name="Test App", bundle="test.app", version="1.0.0",
                                 search_container="c1", search_url="/opensearch"))
            ]
        )

    def test_get_all_with_repository_update_with_auth_source(self):
        # given
        self._setup_descriptor_with_json("descriptor_with_authentication_source.json")

        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = []

        # when
        self.application_service.get_all(repository_update=True)

        # then
        self.application_repository_mock.save.assert_has_calls([
                call(Application(name="Test App", bundle="test.app", version="1.0.0",
                                 auth_source_container="app", auth_source_url="/auth/url"))
            ]
        )

    def test_get_all_with_repository_update_with_configuration(self):
        # given
        self._setup_descriptor_with_json("descriptor_with_configuration.json")

        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = []

        # when
        self.application_service.get_all(repository_update=True)

        # then
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        cont = Container(application=app, name="c1", image="anipfshash/busybox:latest")
        self.application_repository_mock.save.assert_any_call(
            Configuration(container=cont, name="TEST_CONFIG", description="This is a test description")
        )
        cont = Container(application=app, name="c2", image="anipfshash/busybox:latest")
        self.application_repository_mock.save.assert_any_call(
            Configuration(container=cont, name="TEST_CONFIG_2", description="This is a second description")
        )

    def test_get_by_bundle(self):
        # given
        app = Application(name="Test", bundle="test.app", version="1.0.0")
        self.application_repository_mock.find_application_by_bundle.return_value = app

        # when
        result = self.application_service.get_by_bundle("bundle.id")

        # then
        self.assertEqual(app, result)
        self.application_repository_mock.find_application_by_bundle.assert_called_once_with("bundle.id")

    def test_get_application_image(self):
        # given
        self.application_repository_mock.find_application_by_bundle.return_value = \
            Application(name="description", bundle="test.app", version="1.0.0", picture="AQID")

        # when
        image = self.application_service.get_application_image("test.app")

        # then
        self.assertEqual( bytearray([0x01,0x02,0x03]), image)

    def test_get_installed_by_bundle(self):
        # given
        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.installed,
                                                     target_version="1.0.0")
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application

        # when
        returned_installed = self.application_service.get_installed_by_bundle("test.app")

        # then
        self.application_repository_mock.find_installed_application_by_bundle.assert_called_once_with("test.app")
        self.assertEqual(returned_installed, installed_application)

    def test_get_installed_by_bundle_when_never_installed(self):
        # given
        application = Application(bundle="test.app", name="App", version="1.0.0")
        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.uninstalled,
                                                     target_version="1.0.0")
        self.application_repository_mock.find_application_by_bundle.return_value = application
        self.application_repository_mock.find_installed_application_by_bundle.return_value = None

        # when
        returned_installed = self.application_service.get_installed_by_bundle("test.app")

        # then
        self.application_repository_mock.find_installed_application_by_bundle.assert_called_once_with("test.app")
        self.assertEqual(returned_installed, installed_application)

    def test_set_application_state_when_already_installed(self):
        # given
        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.uninstalled,
                                                     target_version="1.0.0")
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application

        # when
        self.application_service.set_application_state("test.app", ApplicationState.downloaded)

        # then
        self.assertEqual(ApplicationState.downloaded, installed_application.target_state)
        self.state_machine_mock.ping.assert_called_once_with()
        self.application_repository_mock.find_application_by_bundle.assert_not_called()

    def test_set_application_state_when_never_installed(self):
        # given
        app = Application(name="Test app", bundle="test.app", version="1.0.0", grant_types="authorization_code,implicit",
                          response_types="id_token,code id_token", token_endpoint_auth_method="client_secret_post",
                          search_container="search_cont", search_url="/opensearch", auth_source_url="/auth/url",
                          auth_source_container="cauth")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = None
        self.application_repository_mock.find_application_by_bundle.return_value = app

        # when
        self.application_service.set_application_state("test.app", ApplicationState.downloaded)

        # then
        self.application_repository_mock.save.assert_called_once_with(InstalledApplication(
            bundle="test.app",
            target_state=ApplicationState.downloaded,
            grant_types="authorization_code,implicit",
            response_types="id_token,code id_token",
            token_endpoint_auth_method="client_secret_post",
            target_version="1.0.0",
            search_container="search_cont",
            search_url="/opensearch",
            auth_source_url="/auth/url",
            auth_source_container="cauth"

        ))

    def test_set_application_configuration_values_when_never_installed(self):
        """
        Ensure that configuration values are correctly initialized on an application that we just
        set the target state for the first time, i.e. when we create the InstalledApplication object
        """
        # given
        app = Application(name="Test app", bundle="test.app", version="1.0.0")
        c1 = Container(application=app, image="test/img", name="c1")
        Configuration(container=c1, name="entry", description="entry description")
        c2 = Container(application=app, image="test/img", name="c2")
        Configuration(container=c2, name="entry2", description="entry description2")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = None
        self.application_repository_mock.find_application_by_bundle.return_value = app

        # when
        self.application_service.set_application_state("test.app", ApplicationState.downloaded)

        # then
        i_app = InstalledApplication(bundle="test.app",
                                     target_version="1.0.0",
                                     current_state=ApplicationState.uninstalled,
                                     target_state=ApplicationState.downloaded)

        self.application_repository_mock.save.assert_any_call(
            ConfigurationValue(installed_application=i_app,
                               container="c1",
                               name="entry",
                               description="entry description")
        )
        self.application_repository_mock.save.assert_any_call(
            ConfigurationValue(installed_application=i_app,
                               container="c2",
                               name="entry2",
                               description="entry description2")
        )

    def test_set_application_state_target_version_attribute_update(self):
        """
        Ensure that installed application attributes are refreshed when an upgrade of the version is requested
        """
        # given
        application = Application(bundle="test.app", version="2.0.0", name="App", grant_types=["grant_types"],
                                  response_types=["response_types"],
                                  token_endpoint_auth_method="client_secret_post",
                                  search_container="cont",
                                  search_url="/search",
                                  auth_source_url = "/auth/url",
                                  auth_source_container = "cauth"
                                 )

        installed_application = InstalledApplication(bundle="test.app", current_version="1.0.0", target_version="1.0.0",
                                                     target_state=ApplicationState.downloaded)
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application
        self.application_repository_mock.find_application_by_bundle.return_value = application

        # when
        self.application_service.set_application_state("test.app", ApplicationState.installed, "2.0.0")

        # then
        self.assertEqual("2.0.0", installed_application.target_version)
        self.application_repository_mock.find_application_by_bundle.assert_called_once_with("test.app")

        self.assertEqual(["response_types"],installed_application.response_types )
        self.assertEqual("client_secret_post", installed_application.token_endpoint_auth_method)
        self.assertEqual(["grant_types"], installed_application.grant_types)
        self.assertEqual("cont", installed_application.search_container)
        self.assertEqual("/search", installed_application.search_url)
        self.assertEqual("/auth/url", installed_application.auth_source_url)
        self.assertEqual("cauth", installed_application.auth_source_container)

    def test_set_application_state_target_version_settings_update(self):
        """
        Ensure that settings are updated on installed containers when an application is updated
        """
        # given
        application = Application(bundle="test.app", version="2.0.0", name="App", grant_types=["grant_types"],
                                  response_types=["response_types"],
                                  token_endpoint_auth_method="client_secret_post",
                                  search_container="cont",
                                  search_url="/search",
                                  auth_source_url = "/auth/url",
                                  auth_source_container = "cauth"
                                  )

        installed_application = InstalledApplication(bundle="test.app", current_version="1.0.0", target_version="1.0.0",
                                                     target_state=ApplicationState.downloaded)
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application
        self.application_repository_mock.find_application_by_bundle.return_value = application

        # when
        self.application_service.set_application_state("test.app", ApplicationState.installed, "2.0.0")

        # then
        self.assertEqual("2.0.0", installed_application.target_version)
        self.application_repository_mock.find_application_by_bundle.assert_called_once_with("test.app")

        self.assertEqual(["response_types"],installed_application.response_types )
        self.assertEqual("client_secret_post", installed_application.token_endpoint_auth_method)
        self.assertEqual(["grant_types"], installed_application.grant_types)
        self.assertEqual("cont", installed_application.search_container)
        self.assertEqual("/search", installed_application.search_url)
        self.assertEqual("/auth/url", installed_application.auth_source_url)
        self.assertEqual("cauth", installed_application.auth_source_container)

    def test_set_application_state_target_version_config_update(self):
        """
        Ensure that settings are updated on installed containers when an application is updated
        """
        # given
        application = Application(bundle="test.app", version="2.0.0", name="App")
        c1 = Container(application=application, image="ipfs/img", name="c1")
        Configuration(container=c1, name="entry1", description="updated desc")  # will be updated
        c2 = Container(application=application, image="ipfs/img", name="c2")
        Configuration(container=c2, name="entry2", description="desc2")         # will be added

        installed_application = InstalledApplication(bundle="test.app", current_version="1.0.0", target_version="1.0.0",
                                                     target_state=ApplicationState.downloaded)
        conf_updated = ConfigurationValue(installed_application=installed_application,
                                       container="c1", name="entry1",value="desc")
        c3 = ConfigurationValue(installed_application=installed_application,     # will be deleted
                           container="c3",
                           name="entry3",
                           value="desc")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application
        self.application_repository_mock.find_application_by_bundle.return_value = application

        # when
        self.application_service.set_application_state("test.app", ApplicationState.installed, "2.0.0")

        # then
        self.application_repository_mock.save.assert_any_call(
            ConfigurationValue(installed_application=installed_application,
                               container="c2", name="entry2", description="desc2")
        )

        self.assertEqual("updated desc", conf_updated.description)

        self.application_repository_mock.delete.assert_called_once_with(c3)

    def test_set_application_state_target_version_not_modified_if_none(self):
        # given
        installed_application = InstalledApplication(bundle="test.app", current_version="1.0.0", target_version="1.0.0",
                                                     target_state=ApplicationState.downloaded)
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application

        # when
        self.application_service.set_application_state("test.app", ApplicationState.installed)

        # then
        self.assertEqual("1.0.0", installed_application.target_version)

    def test_set_application_state_to_version_different_than_catalog(self):
        # given
        application = Application(bundle="test.app", version="2.0.0", name="Test App")
        installed_application = InstalledApplication(bundle="test.app", current_version="1.0.0", target_version="1.0.0",
                                                     target_state=ApplicationState.downloaded)
        self.application_repository_mock.find_application_by_bundle.return_value = application
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application

        # when
        with self.assertRaisesRegex(ValueError, "Target version must match the one of the catalog"):
            self.application_service.set_application_state("test.app", ApplicationState.installed, "3.0.0")

    def test_set_application_with_inexisting_app(self):
        # given
        self.application_repository_mock.find_installed_application_by_bundle.return_value = None
        self.application_repository_mock.find_application_by_bundle.return_value = None

        # when
        self.assertRaises(NotFoundError,
                          self.application_service.set_application_state,"test.app.2", ApplicationState.downloaded)

    def test_get_endpoints_single_exposure(self):
        """
        Test that for a single container with a host_port, a single endpoint is returned, and that
        no endpoint for contents is returned by default
        :return:
        """

        # given
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        installed_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        application_id = Container(application=app, image="anipfshash/busybox:latest", name="c1")
        InstalledContainer(installed_application=installed_app, name="c1", host_port=10001,
                           original_image="ipfs:image", image="image"),
        InstalledContainer(installed_application=installed_app, name="c2",
                           original_image="ipfs:image2", image="image2")
        # this installed content to ensure it will not be returned if asked
        InstalledContent(content_id="c.id", name="cid", installed_application=installed_app, download_path="/a",
                         destination_path="/dest", current_state=ContentState.installed,
                         target_state=ContentState.installed, target_version="1.0.0",
                         endpoint_name="Endpoint", endpoint_url="/url", endpoint_container="c1")

        self.application_repository_mock.find_application_by_bundle.return_value=app
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_app
        self.application_repository_mock.find_container_by_application.return_value = application_id

        # when
        results = self.application_service.get_endpoints("test.app", "https", "localhost")
        
        # then
        if config.PROXY_ENABLE:
            self.assertEqual([Endpoint(name="Test App", url="https://c1.localhost", type=EndpointType.application)],
                              results)
        else:
            self.assertEqual([Endpoint(name="Test App", url="https://localhost:10001", type=EndpointType.application)],
                              results)

    def test_get_endpoints_several_exposures(self):
        """
        Test that in case of several exposed container, the list of endpoints contains each container with
        the name of the container in parenthesis for the name of endpoints
        """
        # given
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        installed_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        application_id = Container(application=app, image="anipfshash/busybox:latest", name="c1")
        Container(application=app, image="anipfshash/busybox:latest", name="c2")
        InstalledContainer(installed_application=installed_app, name="c1", host_port=10001,
                           original_image="image1", image="image"),
        InstalledContainer(installed_application=installed_app, name="c2", host_port=10002,
                           original_image="image2", image="image2")

        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_app
        self.application_repository_mock.find_container_by_application.return_value = application_id

        # when
        results = self.application_service.get_endpoints("test.app", "https", "localhost")

        # then
        if config.PROXY_ENABLE:
            self.assertEqual([Endpoint(name="Test App (c1)", url="https://c1.localhost", type=EndpointType.application),
                              Endpoint(name="Test App (c2)", url="https://c2.localhost", type=EndpointType.application)],
                              results)
        else:
            self.assertEqual([Endpoint(name="Test App (c1)", url="https://localhost:10001", type=EndpointType.application),
                              Endpoint(name="Test App (c2)", url="https://localhost:10002", type=EndpointType.application)],
                              results)

    def test_get_endpoints_with_content_endpoints(self):
        """
        Test that the list of endpoints includes only a content exposed as an endpoint if requested with with_contents
        and without with_containers
        """
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        installed_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        application_id = Container(application=app, image="anipfshash/busybox:latest", name="c1")
        InstalledContainer(installed_application=installed_app, name="test.app.c1", host_port=10001,
                           original_image="ipfs:image", image="image")
        InstalledContent(content_id="c.id", name="cid", installed_application=installed_app, download_path="/a",
                         destination_path="/dest", current_state=ContentState.installed,
                         target_state=ContentState.installed, target_version="1.0.0",
                         endpoint_name="Endpoint", endpoint_url="/url", endpoint_container="c1")

        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_app
        self.application_repository_mock.find_container_by_application.return_value = application_id
        
        # when
        results = self.application_service.get_endpoints("test.app", "https", "localhost",
                                                            with_contents=True, with_containers=False)
        # then  
        if config.PROXY_ENABLE:
            self.assertEqual(results,
                            [Endpoint(name="Endpoint", url="https://c1.localhost/url", type=EndpointType.content)]
                            )
        else:
            self.assertEqual(results,
                            [Endpoint(name="Endpoint", url="https://localhost:10001/url", type=EndpointType.content)]
                            )

    def test_get_endpoints_with_category_filter(self):
        """
        Test that the list of endpoints includes only a content included in a specified category if category_id is
        specified
        """
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        installed_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        application_id = Container(application=app, image="anipfshash/busybox:latest", name="c1")
        ic1 = InstalledContainer(installed_application=installed_app, name="test.app.c1", host_port=10001,
                           original_image="ipfs:image", image="image")
        ic2 = InstalledContent(content_id="c.id", name="cid", installed_application=installed_app, download_path="/a",
                         destination_path="/dest", current_state=ContentState.installed,
                         target_state=ContentState.installed, target_version="1.0.0",
                         endpoint_name="Endpoint", endpoint_url="/url", endpoint_container="c1")
        ic3 = InstalledContent(content_id="c.id2", name="cid", installed_application=installed_app, download_path="/a",
                               destination_path="/dest", current_state=ContentState.installed,
                               target_state=ContentState.installed, target_version="1.0.0",
                               endpoint_name="Endpoint", endpoint_url="/url", endpoint_container="c1")
        c = Category(id=123)
        c.installed_contents.append(ic2)

        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_app
        self.application_repository_mock.find_container_by_application.return_value = application_id
        self.category_service_mock.get_category_by_id.return_value = c

        # when
        results = self.application_service.get_endpoints("test.app", "https", "localhost",
                                                        with_contents=True, with_containers=True, category_id=123)
        # then
        if config.PROXY_ENABLE:
            self.category_service_mock.get_category_by_id.assert_called_once_with(123)
            self.assertEqual(results,
                            [Endpoint(name="Endpoint", url="https://c1.localhost/url", type=EndpointType.content)])

        else:
            self.category_service_mock.get_category_by_id.assert_called_once_with(123)
            self.assertEqual(results,
                            [Endpoint(name="Endpoint", url="https://localhost:10001/url", type=EndpointType.content)])



    def test_get_endpoints_with_playlist_filter(self):
        """
        Test that the list of endpoints includes only a content included in a specified category if category_id is
        specified
        """
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        installed_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        application_id = Container(application=app, image="anipfshash/busybox:latest", name="c1")
        ic1 = InstalledContainer(installed_application=installed_app, name="test.app.c1", host_port=10001,
                                 original_image="ipfs:image", image="image")
        ic2 = InstalledContent(content_id="c.id", name="cid", installed_application=installed_app, download_path="/a",
                               destination_path="/dest", current_state=ContentState.installed,
                               target_state=ContentState.installed, target_version="1.0.0",
                               endpoint_name="Endpoint", endpoint_url="/url", endpoint_container="c1")
        ic3 = InstalledContent(content_id="c.id2", name="cid", installed_application=installed_app, download_path="/a",
                               destination_path="/dest", current_state=ContentState.installed,
                               target_state=ContentState.installed, target_version="1.0.0",
                               endpoint_name="Endpoint", endpoint_url="/url", endpoint_container="c1")
        c = Playlist(id=123, title="My playlist", user=User(username="test", name="test"))
        c.installed_contents.append(ic2)

        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_app
        self.application_repository_mock.find_container_by_application.return_value = application_id
        self.category_service_mock.get_playlist_by_id.return_value = c

        # when
        results = self.application_service.get_endpoints("test.app", "https", "localhost",
                                                         with_contents=True, with_containers=True, playlist_id=123)

        # then
        if config.PROXY_ENABLE:
            self.category_service_mock.get_playlist_by_id.assert_called_once_with(123)
            self.assertEqual(results,
                            [Endpoint(name="Endpoint", url="https://c1.localhost/url", type=EndpointType.content)])
        else:
            self.category_service_mock.get_playlist_by_id.assert_called_once_with(123)
            self.assertEqual(results,
                            [Endpoint(name="Endpoint", url="https://localhost:10001/url", type=EndpointType.content)])

    def test_get_contents_for_bundle(self):
        # given
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        c = Content(content_id="c1", name="Content 1", description="desc", application=app,
                    download_path="/ipfs/ABCD", destination_path="/file", version="1.0.0")
        ic = InstalledContent(content_id="c1", name="Content 1", installed_application=i_app,
                              destination_path="/file", current_state=ContentState.uninstalled,
                              download_path="/ipfs/path", target_state=ContentState.installed,
                              target_version="1.0.0")

        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        response = self.application_service.get_contents_for_bundle("test.app")

        # then
        self.application_repository_mock.find_application_by_bundle.assert_called_once_with("test.app")
        self.assertEqual([ (c, ic) ], response)

    def test_get_contents_for_bundle_with_only_installed(self):
        """
        Test that we are able to return the installed content even if the corresponding content has been uninstalled
        from the repository
        """
        # given
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        ic = InstalledContent(content_id="c1", name="Content 1", installed_application=i_app,
                              destination_path="/file", current_state=ContentState.installed,
                              download_path="/ipfs/path", target_state=ContentState.installed,
                              target_version="1.0.0")

        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        response = self.application_service.get_contents_for_bundle("test.app")

        # then
        self.application_repository_mock.find_application_by_bundle.assert_called_once_with("test.app")
        self.assertEqual( [(None, ic)], response)

    def test_get_contents_for_bundle_with_only_uninstalled(self):
        """
        Test that we don't return an installed content where it's content is missing if the state is uninstalled
        """
        # given
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        ic = InstalledContent(content_id="c1", name="Content 1", installed_application=i_app,
                              destination_path="/file", current_state=ContentState.uninstalled,
                              download_path="/ipfs/path", target_state=ContentState.uninstalled,
                              target_version="1.0.0")

        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        response = self.application_service.get_contents_for_bundle("test.app")

        # then
        self.application_repository_mock.find_application_by_bundle.assert_called_once_with("test.app")
        self.assertEqual([], response)

    def test_get_contents_for_bundle_not_installed(self):
        """
        Test default values of the InstalledContent object returned by the method get_contents_for_bundle
        when the content has not yet been installed
        """
        # given
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        Content(content_id="c1", name="Content 1", description="desc", application=app,
                    download_path="/ipfs/ABCD", destination_path="/file", version="1.0.0",
                    endpoint_container="c1", endpoint_url="/url", endpoint_name="endpoint name")

        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        response = self.application_service.get_contents_for_bundle("test.app")

        # then
        self.application_repository_mock.find_application_by_bundle.assert_called_once_with("test.app")

        # do not use object equality for the asertion, as it may break initial conditions because
        # of sqlalchemy relationsips
        (c, ic) = response[0]
        self.assertEqual("c1", ic.content_id)
        self.assertEqual("Content 1", ic.name)
        self.assertEqual("/ipfs/ABCD", ic.download_path)
        self.assertEqual("/file", ic.destination_path)
        self.assertEqual(i_app, ic.installed_application)
        self.assertEqual(ContentState.uninstalled, ic.current_state)
        self.assertEqual(ContentState.uninstalled, ic.target_state)
        self.assertEqual("1.0.0", ic.target_version)
        self.assertEqual("c1", ic.endpoint_container)
        self.assertEqual("/url", ic.endpoint_url)
        self.assertEqual("endpoint name", ic.endpoint_name)
        self.assertIsNone(ic.current_version)
        self.application_repository_mock.save.assert_not_called()

    def test_get_content_for_bundle_and_content_id(self):
        # given
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        c = Content(content_id="c1", name="Content 1", description="desc", application=app,
                    download_path="/ipfs/ABCD", destination_path="/file", version="1.0.0")
        ic = InstalledContent(content_id="c1", name="Content 1", installed_application=i_app,
                              destination_path="/file", current_state=ContentState.uninstalled,
                              download_path="/ipfs/path", target_state=ContentState.uninstalled,
                              target_version="1.0.0")

        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        response = self.application_service.get_content_for_bundle_and_content_id("test.app", "c1")

        # then
        self.assertEqual( (c, ic) , response)

    def test_get_installed_content_for_bundle_and_content_id(self):
        # given
        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.installed,
                                                     target_version="1.0.0")
        ic = InstalledContent(name="Content", content_id="c1", installed_application=installed_application,
                              destination_path="dest_path", current_state=ContentState.installed,
                              download_path="/ipfs/path", target_state=ContentState.installed)

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application

        # when
        response = self.application_service.get_installed_content_for_bundle_and_content_id("test.app", "c1")

        # then
        self.assertEqual(response, ic)

    def test_get_installed_content_for_bundle_and_content_id(self):
        # given
        # app without an installed content
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        c = Content(content_id="c1", name="Content 1", description="desc", application=app,
                    download_path="/ipfs/ABCD", destination_path="/file", version="1.0.0")
        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.installed,
                                                     target_version="1.0.0")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application
        self.application_repository_mock.find_application_by_bundle.return_value = app

        # when
        response = self.application_service.get_installed_content_for_bundle_and_content_id("test.app", "c1")

        # then
        ic = InstalledContent(name="Content 1", content_id="c1", installed_application=installed_application,
                              destination_path="/file", current_state=ContentState.uninstalled,
                              download_path="/ipfs/ABCD", target_state=ContentState.uninstalled,
                              target_version="1.0.0")
        self.assertEqual(response, ic)

    def test_set_target_state_for_content_id(self):
        # given
        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.installed,
                                                     target_version="1.0.0")
        ic = InstalledContent(name="Content", content_id="c1", installed_application=installed_application,
                              destination_path="dest_path", current_state=ContentState.uninstalled,
                              download_path="/ipfs/ABCD", target_state=ContentState.uninstalled, target_version="1.0.0")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application
        self.application_repository_mock.find_application_by_bundle.return_value = None
        self.application_repository_mock.find_content_with_different_state_or_different_version.return_value = list()

        # when
        self.application_service.set_target_state_for_content_id("test.app", "c1", ContentState.installed)

        # then
        self.assertEqual(ContentState.installed, ic.target_state)
        self.assertEqual("1.0.0", ic.target_version)

    def test_set_target_state_for_content_id_with_version(self):
        # given
        app = Application(name="Test App", bundle="test.app", version="1.0.0")

        Content(content_id="c1", name="Content 1", application=app,
                download_path="/ipfs/ABCD", destination_path="/file", version="2.0.0")
        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.installed,
                                                     target_version="1.0.0")
        ic = InstalledContent(id=123, name="Content", content_id="c1", installed_application=installed_application,
                              destination_path="dest_path", current_state=ContentState.uninstalled,
                              download_path="/ipfs/ABCD", target_state=ContentState.uninstalled, target_version="1.0.0")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application
        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_content_with_different_state_or_different_version.return_value = list()

        # when
        self.application_service.set_target_state_for_content_id("test.app", "c1", ContentState.installed, "2.0.0")

        # then
        self.assertEqual("2.0.0", ic.target_version)

    def test_set_target_state_for_content_id_if_never_installed(self):
        # given
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        Content(content_id="c1", name="Content 1", application=app,
                download_path="/ipfs/ABCD", destination_path="/file", version="1.0.0")

        # installed app without installed content
        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.installed,
                                                     target_version="1.0.0")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application
        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_content_with_different_state_or_different_version.return_value = list()

        # when
        self.application_service.set_target_state_for_content_id("test.app", "c1", ContentState.installed)

        # then
        self.application_repository_mock.save.assert_called_once_with(
            InstalledContent(name="Content 1", content_id="c1", destination_path="/file",
                             current_state=ContentState.uninstalled, target_state=ContentState.installed,
                             download_path="/ipfs/ABCD", installed_application=installed_application,
                             target_version="1.0.0")
        )

    def test_set_target_state_for_content_id_version_not_in_catalog(self):
        # given
        app = Application(name="Test App", bundle="test.app", version="1.0.0")

        Content(content_id="c1", name="Content 1", application=app,
                download_path="/ipfs/ABCD", destination_path="/file", version="2.0.0")

        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.installed,
                                                     target_version="1.0.0")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application
        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_content_with_different_state_or_different_version.return_value = list()

        # when
        self.assertRaises(ValueError, self.application_service.set_target_state_for_content_id, "test.app", "c1",
                          ContentState.installed, version="3.0.0")

    def test_set_target_state_for_content_id_version_not_in_catalog_installed_content(self):
        # given
        app = Application(name="Test App", bundle="test.app", version="1.0.0")

        Content(content_id="c1", name="Content 1", application=app,
                download_path="/ipfs/ABCD", destination_path="/file", version="2.0.0")

        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.installed,
                                                     target_version="1.0.0")

        InstalledContent(name="Content", content_id="c1", installed_application=installed_application,
                              destination_path="dest_path", current_state=ContentState.uninstalled,
                              download_path="/ipfs/ABCD", target_state=ContentState.uninstalled, target_version="1.0.0")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = installed_application
        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_content_with_different_state_or_different_version.return_value = list()

        # when
        self.assertRaises(ValueError, self.application_service.set_target_state_for_content_id, "test.app", "c1",
                          ContentState.installed, version="3.0.0")

    def test_authenticate_application(self):
        # given
        config.OAUTH2_DEV_MODE = False
        i_app = InstalledApplication(bundle="test.app", client_secret="hashed_password", target_version="2.0.0")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app
        self.password_hasher_mock.verify.return_value = True

        # when
        authenticated = self.application_service.authenticate_application("test.app", "password")

        # then
        self.assertTrue(authenticated)
        self.application_repository_mock.find_installed_application_by_bundle.assert_called_once_with("test.app")
        self.password_hasher_mock.verify.assert_called_once_with("password", "hashed_password")

    def test_wrong_authentication_for_application(self):
        # given
        config.OAUTH2_DEV_MODE = False
        i_app = InstalledApplication(bundle="test.app", client_secret="hashed_password", target_version="1.0.0")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app
        self.password_hasher_mock.verify.return_value = False

        # when
        authenticated = self.application_service.authenticate_application("test.app", "password")

        # then
        self.assertFalse(authenticated)

    def test_application_authentication_with_dev_mode(self):
        # given
        config.OAUTH2_DEV_MODE = True
        i_app = InstalledApplication(bundle="test.app", client_secret="hashed_password", target_version="1.0.0")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app
        self.password_hasher_mock.verify.return_value = False

        # when
        authenticated = self.application_service.authenticate_application("test.app", "test.app")

        # then
        self.assertTrue(authenticated)

    def test_set_display_settings(self):
        # given
        i_app = InstalledApplication(bundle="test.app", client_secret="hashed_password",
                                     target_version="1.0.0", visible=True)
        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        self.application_service.set_display_settings("test.app", False, 3)

        # then
        self.application_repository_mock.find_installed_application_by_bundle.assert_called_once_with("test.app")
        self.assertFalse(i_app.visible)
        self.assertEqual(3, i_app.display_weight)

    def test_sync_terms(self):
        # given
        app = Application(name="Test", bundle="test.app", version="1.0.0")
        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.get_all.return_value = []
        self.application_repository_mock.get_all_installed.return_value = []
        self._setup_descriptor_with_json({
            'applications': [],
            'terms' :['term1', 'term2', 'term3']
        })

        # when
        self.application_service.get_all(repository_update=True)

        # then
        self.category_service_mock.import_terms.assert_called_once_with(['term1', 'term2', 'term3'])

    def test_auth_source_applications(self):
        """
        Assert that the auth_source_applications method returns the list of applications with
        an auth source container, in the form of a list of tuples (Application, InstalledApplication)
        """
        # given
        app = Application(bundle="test.app", name="Test App", version="1.0.0")

        i_app = InstalledApplication(bundle="test.app", auth_source_container="c1", auth_source_url="/auth/url",
                                     auth_source_secret="abc", target_version="1.0.0", visible=True)

        self.application_repository_mock.find_application_by_bundle.return_value = app
        self.application_repository_mock.find_inst_app_with_auth_source_container.return_value = [i_app]

        # when
        apps = self.application_service.auth_source_installed_application()

        # then
        self.assertEqual([(app, i_app)], apps)
        self.application_repository_mock.find_inst_app_with_auth_source_container.assert_called_once_with(
            current_state=ApplicationState.installed
        )
        self.application_repository_mock.find_application_by_bundle.assert_called_once_with("test.app")

    def test_update_configuration(self):
        """
        Ensure that an application configuration is updated correctly
        """

        # given
        i_app = InstalledApplication(bundle="test.app",
                                     target_version="1.0.0",
                                     current_state=ApplicationState.installed)

        i_conf = ConfigurationValue(name="entry",
                                    description="entry description",
                                    value="val",
                                    container="c1",
                                    installed_application=i_app)

        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        parsed_conf = ConfigParameter(name="entry", value="val2")
        cont_config = ContainerConfig(container="c1", parameters=[parsed_conf])
        self.application_service.update_configuration("test.app", [cont_config])

        # then
        self.assertEqual("val2", i_conf.value)
        self.application_repository_mock.find_installed_application_by_bundle.assert_called_once_with("test.app")

    def test_update_configuration_with_wrong_container_name(self):
        """
        Ensure that an application configuration update fails if the specified container doesn't exits in
        the database
        """

        # given
        i_app = InstalledApplication(bundle="test.app",
                                     target_version="1.0.0",
                                     current_state=ApplicationState.installed)

        i_cont = InstalledContainer(name="test.app.c1", installed_application=i_app,
                                    original_image="ipfs:test/image", image="test/image")

        i_conf = ConfigurationValue(name="entry", description="entry description", value="val", container="c1",
                                    installed_application=i_app)

        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        parsed_conf = ConfigParameter(name="entry", value="val2")
        cont_config = ContainerConfig(container="c2", parameters=[parsed_conf])
        with self.assertRaises(ValueError):
            self.application_service.update_configuration("test.app", [cont_config])

    def test_update_configuration_with_wrong_param_name(self):
        """
        Ensure that an application configuration update fails if a specified parameter doesn't match
        a possible configuration value in the descriptor
        """

        # given
        i_app = InstalledApplication(bundle="test.app",
                                     target_version="1.0.0",
                                     current_state=ApplicationState.installed)

        i_cont = InstalledContainer(name="test.app.c1", installed_application=i_app,
                                    original_image="ipfs:test/image", image="test/image")

        i_conf = ConfigurationValue(name="entry", description="entry description", value="val", container="c1",
                                    installed_application=i_app)

        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        parsed_conf = ConfigParameter(name="wrong_entry", value="val2")
        cont_config = ContainerConfig(container="c1", parameters=[parsed_conf])
        with self.assertRaises(ValueError):
            self.application_service.update_configuration("test.app", [cont_config])

    def test_update_configuration_app_not_found(self):
        """
        Ensure that an application configuration is updated correctly
        """

        # given
        self.application_repository_mock.find_installed_application_by_bundle.return_value = None

        # when
        parsed_conf = ConfigParameter(name="entry", value="val2")
        cont_config = ContainerConfig(container="c1", parameters=[parsed_conf])
        with self.assertRaises(NotFoundError):
            self.application_service.update_configuration("test.app", [cont_config])

    def _setup_descriptor_with_json(self, json):
        if isinstance(json,str):
            descriptor = self.load_json_resource(json)
            self.storage_mock.download_descriptor.return_value=descriptor
        else:
            self.storage_mock.download_descriptor.return_value=json
