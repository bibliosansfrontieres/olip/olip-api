from authlib.flask.oauth2 import ResourceProtector
from flask import request
from flask_restplus import Namespace, Resource, fields, inputs
from werkzeug.exceptions import BadRequest

from app.tools.request_tools import ResourceWithLinks, protected_resource, message
from app.users.service import UserService
from injector import inject
from flask_restplus import reqparse

import app.tools.data_conversion as data_conversion

authorizations = {
    'Bearer': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

user_api = Namespace('users', description="API managing users", authorizations=authorizations)

link_fields = user_api.model('UserLink', {
    'rel': fields.String(description="identifier of the link"),
    'href': fields.String(description="url of the link")
})

user_model = user_api.model('User', {
    'provider': fields.String(description="External provider of the user if not internal"),
    'username': fields.String(description="Unique identifier for a user"),
    'name': fields.String(description="Full name of the user"),
    'admin': fields.Boolean(description="True or false, whether the user is an administrator"),
    'links': fields.List(fields.Nested(link_fields))
})


def to_resource_link(user):
    r = ResourceWithLinks(user, user_api)
    r.add_link("self", user.get_user_id())

    return r


@user_api.route("/")
class UserListResource(Resource):
    """
    Returns the list of users
    """

    @inject
    def __init__(self, user_service: UserService, resource_protector: ResourceProtector, api):
        self.user_service = user_service
        self.resource_protector = resource_protector
        self.api=api

    @user_api.marshal_with(user_model, envelope="data", skip_none=True)
    @protected_resource(role="admin")
    def get(self):
        """Gets the list of all users declared on the platform"""
        users = self.user_service.get_all()

        user_map = list(map(lambda u: to_resource_link(u),users))

        return user_map


user_parser = reqparse.RequestParser()
user_parser.add_argument('name', help="User's full name", required=True)
user_parser.add_argument('admin', type=inputs.boolean, help="True or false, whether the user is an administrator", required=False)
user_parser.add_argument('password', help="User's password", required=False)


@user_api.route("/<string:username>")
@user_api.doc(params={'username': 'unique id of the user'})
class UserResource(Resource):
    """
    Returns the representation of a specific user or create/update it
    """
    @inject
    def __init__(self, user_service: UserService, resource_protector: ResourceProtector, api):
        self.user_service = user_service
        self.resource_protector = resource_protector
        self.api=api

    @user_api.marshal_with(user_model, skip_none=True)
    @protected_resource(role="admin")
    def get(self, username):
        """
        Gets a specific user
        """
        provider, username = data_conversion.sub_to_username_and_provider(username)

        user = self.user_service.get_by_username_and_provider(username, provider=provider)

        return to_resource_link(user)

    @user_api.marshal_with(user_model, skip_none=True)
    @protected_resource(role="admin")
    def delete(self, username):
        """
        Deletes a specific user
        """
        provider, username = data_conversion.sub_to_username_and_provider(username)

        if username == "admin":
            return message("The admin user cannot be deleted")

        user = self.user_service.get_by_username_and_provider(username, provider=provider)
        self.user_service.delete_user(user)

        return message("User deleted")

    @user_api.expect(user_parser)
    @user_api.marshal_with(user_model, skip_none=True)
    @user_api.doc(security=authorizations)
    @protected_resource(role="admin")
    def put(self, username):
        """
        Create or update a user.
        The password must be set, but will be hashed before storage
        """
        args = user_parser.parse_args(request)

        if '|' in username:
            raise BadRequest('Incorrect user name')

        admin = False

        if "admin" in args:
            admin = args["admin"]

        user = self.user_service.create_or_update_user(username, args["name"], admin, args["password"])

        return to_resource_link(user)
