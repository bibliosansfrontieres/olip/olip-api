import config

from unittest import TestCase
from unittest.mock import Mock, call

from app.applications import ApplicationRepository
from app.applications.models import InstalledApplication, InstalledContent, ContentState, Application, Content
from app.applications.state_machine.upgrade_content_task import UpgradeContentTask
from app.applications.storage.storage import Storage
from app.applications.storage.storage_manager import StorageManager



class UpgradeContentTaskTest(TestCase):

    def setUp(self):
        self.storage_manager_mock = Mock(spec=StorageManager)
        self.storage_mock = Mock(spec=Storage)
        self.storage_manager_mock.storage_for_url.return_value = self.storage_mock

        self.application_repository_mock = Mock(spec=ApplicationRepository)

        self.upgrade_content_task = UpgradeContentTask(self.application_repository_mock, config,
                                                       self.storage_manager_mock)
    def test_upgrade_content(self):
        # given
        config.HOST_DATA_DIR="/data"
        self.upgrade_content_task.set_content(bundle_id="bundle.id", content_id="content.id")
        ia = InstalledApplication(bundle="bundle.id", target_version="1.0.0", has_content_to_upgrade=True)
        ic = InstalledContent(content_id="content.id", installed_application=ia, name="Content", size=1,
                              destination_path="a/folder/file", current_state=ContentState.installed,
                              download_path="ipfs:/ipfs/path", target_state=ContentState.installed,
                              current_version="0.0.1", target_version="1.0.0")

        a = Application( name="App", bundle="bundle.id", version="1.0.0")
        Content(content_id="content.id", name="Content", application=a, download_path="ipfs:/ipfs/newpath",
                    destination_path="a/new/folder/file", version="1.0.0")

        self.application_repository_mock.find_application_by_bundle.return_value = a
        self.application_repository_mock.find_installed_application_by_bundle.return_value = ia

        # need to check the calls in order for that specific use case
        manager = Mock()
        manager.attach_mock(self.storage_manager_mock, 'manager')
        manager.attach_mock(self.storage_mock, 'storage')

        # when
        self.upgrade_content_task.run()

        self.application_repository_mock.find_application_by_bundle('bundle.id')
        self.application_repository_mock.find_installed_application_by_bundle('bundle.id')

        # then

        expected_calls = [
            call.manager.storage_for_url("ipfs:/ipfs/path"),
            call.storage.remove_content("ipfs:/ipfs/path", "a/folder/file", "/data/bundle.id/content"),
            call.manager.storage_for_url("ipfs:/ipfs/newpath"),
            call.storage.download_content("ipfs:/ipfs/newpath", "a/new/folder/file", "/data/bundle.id/content"),
        ]

        manager.assert_has_calls(expected_calls)

        self.assertEqual(ic.current_version, "1.0.0")
        self.application_repository_mock.commit.assert_called_once_with()
        self.assertFalse(ia.has_content_to_upgrade)



