import os

from unittest.mock import Mock, call

from sqlalchemy.orm.exc import NoResultFound


from app.logo.models import Logo
from app.logo.repository import LogoRepository
from app.logo.service import LogoService
from app.tools.errors import ForbiddenError, NotFoundError
from test.unit.abstract_unit_test import AbstractUnitTest


class LogoServiceTest(AbstractUnitTest):

    def setUp(self):
        self.logo_repository = Mock(spec=LogoRepository)
        self.logo_service = LogoService(self.logo_repository)

    def test_set_logo_thumbnail(self):

        # given
        logo = Logo(id=1)
        self.logo_repository.get_logo.return_value = logo

        # when
        self.logo_service.set_logo_thumbnail("image/png", b"1")

        # then
        self.assertEqual('image/png', logo.thumbnail_mime_type)
        self.assertEqual(b"1", logo.thumbnail)

    def test_get_thumbnail_for_logo(self):
        # """ Ensure that a category thumbnail is set when the set_thumbnail_for_category is called """
        # given
        logo = Logo(id=1)
        logo.thumbnail_mime_type = "image/jpg"
        logo.thumbnail = b"abcde"
        self.logo_repository.get_logo.return_value = logo

        # when
        content_type, thumbnail = self.logo_service.get_logo_thumbnail()

        # then
        self.logo_repository.get_logo.assert_called_once_with(id=1)
        self.assertEqual('image/jpg', content_type)
        self.assertEqual(b"abcde", thumbnail)
