import config

from unittest import TestCase
from unittest.mock import Mock

from app.applications import ApplicationRepository
from app.applications.models import InstalledApplication, InstalledContent, ContentState, Application, Content
from app.applications.state_machine.install_content_task import InstallContentTask
from app.applications.storage.storage import Storage
from app.applications.storage.storage_manager import StorageManager

class InstallContentTaskTest(TestCase):

    def setUp(self):
        self.storage_manager_mock = Mock(spec=StorageManager)
        self.storage_mock = Mock(spec=Storage)
        self.storage_manager_mock.storage_for_url.return_value = self.storage_mock

        self.application_repository_mock = Mock(spec=ApplicationRepository)

        self.install_content_task = InstallContentTask(self.application_repository_mock, config,
                                                       self.storage_manager_mock)
