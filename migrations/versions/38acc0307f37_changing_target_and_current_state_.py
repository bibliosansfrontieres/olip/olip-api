"""changing target and current state column type

Revision ID: 38acc0307f37
Revises: 2427ff551ced
Create Date: 2022-01-10 10:02:52.436490

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '38acc0307f37'
down_revision = '2427ff551ced'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('installed_contents', schema=None) as step1Up:
        step1Up.drop_constraint('contentstate', type_='check')
        step1Up.create_check_constraint("ck_installed_contents_current_state", "current_state IN ('uninstalled', 'installed', 'broken')")
        step1Up.create_check_constraint("ck_installed_contents_target_state", "target_state IN ('uninstalled', 'installed', 'broken')")

def downgrade():
     with op.batch_alter_table('installed_contents', schema=None) as step1Up:
        step1Up.drop_constraint('ck_installed_contents_current_state', type_='check')
        step1Up.drop_constraint('ck_installed_contents_target_state', type_='check')
        step1Up.create_check_constraint("ck_current_state", "current_state IN ('uninstalled', 'installed')")
        step1Up.create_check_constraint("ck_target_state", "target_state IN ('uninstalled', 'installed')")