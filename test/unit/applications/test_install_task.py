from unittest import TestCase
from unittest.mock import Mock, call

from app.applications import ApplicationRepository
from app.applications.models import Application, Container, InstalledApplication, ApplicationState, InstalledContainer, \
    InstalledContent, ContentState, Content, ConfigurationValue
from app.applications.state_machine.install_task import InstallTask, FreePortFinder, NoFreePort
from app.applications.storage.storage import Storage
from app.applications.storage.storage_manager import StorageManager
from app.core.docker_client import DockerClient

import config
from app.core.filesystem import Filesystem
from app.core.password_hasher import PasswordHasher
from app.platform import PlatformService


class FreePortFinderTest(TestCase):

    """
    Test of the freeport finder class
    """

    def setUp(self):
        self.application_repository_mock = Mock(spec=ApplicationRepository)

        self.free_port_finder = FreePortFinder(self.application_repository_mock, config)

    def test_next_port(self):
        """
        Ensure that a correct port is generated when there is existing installed containers in the db
        """
        # given
        app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        self.application_repository_mock.find_inst_container_by_port_number.side_effect = [
            InstalledContainer(name="test", installed_application=app, host_port=10000,
                               original_image="ipfs:image1",image="image1"),
            InstalledContainer(name="test", installed_application=app, host_port=10001,
                               original_image="ipfs:image2", image="image2"),
            None
        ]

        # when
        port = self.free_port_finder.find_free_port()

        # then
        self.assertEqual(10002, port)

    def test_upper_bound(self):
        """
        Ensure that no container can be created if there is no free port after the upper bound configured
        in the settings
        """
        # given
        config.APPLICATIONS_PORT_RANGE = '10000-10001'
        app = InstalledApplication(bundle="test.app", target_version="1.0.0")

        self.application_repository_mock.find_inst_container_by_port_number.side_effect = [
            InstalledContainer(name="test", installed_application=app, host_port=10000,
                               original_image="ipfs:image1", image="image1"),
            InstalledContainer(name="test", installed_application=app, host_port=10001,
                               original_image="ipfs:image2", image="image2")
        ]

        # when
        self.assertRaises(NoFreePort, self.free_port_finder.find_free_port)

        # then
        self.assertEqual(2, self.application_repository_mock.find_inst_container_by_port_number.call_count)


class InstallTaskTest(TestCase):

    def setUp(self):
        self.application_repository = Mock(spec=ApplicationRepository)
        self.docker_client = Mock(spec=DockerClient)
        self.free_port_finder = Mock(spec=FreePortFinder)
        self.filesystem_mock = Mock(spec=Filesystem)
        self.password_hasher_mock = Mock(spec=PasswordHasher)
        self.platform_service_mock = Mock(spec=PlatformService)
        self.storage_manager_mock = Mock(spec=StorageManager)
        self.storage_mock = Mock(spec=Storage)
        self.storage_manager_mock.storage_for_url.return_value = self.storage_mock
        self.storage_mock.get_protocol.return_value = "ipfs"

        self.install_task = InstallTask(self.application_repository, self.docker_client,
                                        self.free_port_finder, config, self.filesystem_mock, self.password_hasher_mock,
                                        self.platform_service_mock, self.storage_manager_mock)

        self.install_task.set_bundle("test.app")

    def test_network_creation(self):
        """
        Ensure that a docker network is created when installing an application
        """

        # given

        app = Application(name="Test App", bundle="test.app", version="1.0.0")  # app without containers

        installed_app = InstalledApplication(bundle="test.app",
                                             current_state=ApplicationState.downloaded,
                                             target_state=ApplicationState.installed,
                                             target_version="1.0.0")

        self.application_repository.find_application_by_bundle.return_value = app
        self.application_repository.find_installed_application_by_bundle.return_value = installed_app
        self.docker_client.network_exists.return_value = False

        # when
        self.install_task.run()

        # then
        self.docker_client.network_exists.assert_called_once_with("test.app")
        self.docker_client.create_network.assert_called_once_with("test.app")

    def test_no_network_creation_if_already_exists(self):
        """
        Ensure that no docker network is created if it already exists
        """
        # given

        app = Application(name="Test App", bundle="test.app", version="1.0.0")  # app without containers

        installed_app = InstalledApplication(bundle="test.app",
                                             current_state=ApplicationState.downloaded,
                                             target_state=ApplicationState.installed,
                                             target_version="1.0.0")

        self.application_repository.find_application_by_bundle.return_value = app
        self.application_repository.find_installed_application_by_bundle.return_value = installed_app
        self.docker_client.network_exists.return_value = True

        # when
        self.install_task.run()

        # then
        self.docker_client.create_network.assert_not_called()

    def test_client_secret_update(self):
        """
        Ensure that Openid connect environment variable are set in the container's environment
        """
        # given

        app = Application(name="Test App", bundle="test.app", version="1.0.0")  # app without containers

        installed_app = InstalledApplication(bundle="test.app",
                                             current_state=ApplicationState.downloaded,
                                             target_state=ApplicationState.installed,
                                             token_endpoint_auth_method="client_secret_basic",
                                             target_version="1.0.0")
        app.containers = [
            Container(image="repo/image:latest", name="c1", application=app, expose=1234)
        ]

        self.application_repository.find_application_by_bundle.return_value = app
        self.application_repository.find_installed_application_by_bundle.return_value = installed_app
        self.password_hasher_mock.random_password.return_value = "generated_password"
        self.password_hasher_mock.hash.return_value = "hashed_password"
        self.platform_service_mock.get_api_base_url.return_value = "http://10.0.0.3:8082"
        self.free_port_finder.find_free_port.return_value=10001

        # when
        self.install_task.run()

        # then
        self.password_hasher_mock.random_password.assert_called_once_with()
        self.password_hasher_mock.hash.assert_called_once_with("generated_password")
        self.assertEqual(1, self.docker_client.create_container.call_count)

        for k in self.docker_client.create_container.call_args[1]:
            if k == 'environment':
                self.assertEqual({
                  'APPLICATION_ROOT': 'localhost:10001',
                  'CLIENT_ID': 'test.app',
                  'CLIENT_SECRET': 'generated_password',
                  'OIDC_URL': 'http://10.0.0.3:8082'
                },self.docker_client.create_container.call_args[1][k])

        self.assertEqual("hashed_password", installed_app.client_secret)

    def test_auth_source_secret_update(self):
        """
        When an application has an authentication source, ensure that a AUTH_SOURCE_SECRET environment variable
        is populated so that delegated authentication is secured
        :return:
        """
        # given

        app = Application(name="Test App", bundle="test.app", version="1.0.0")  # app without containers

        installed_app = InstalledApplication(bundle="test.app",
                                             current_state=ApplicationState.downloaded,
                                             target_state=ApplicationState.installed,
                                             auth_source_container="app",
                                             auth_source_url="/auth/url",
                                             target_version="1.0.0")
        app.containers = [
            Container(image="repo/image:latest", name="c1", application=app, expose=1234)
        ]

        self.application_repository.find_application_by_bundle.return_value = app
        self.application_repository.find_installed_application_by_bundle.return_value = installed_app
        self.password_hasher_mock.random_password.return_value = "generated_password"
        self.platform_service_mock.get_api_base_url.return_value = "http://10.0.0.3:8082"
        self.free_port_finder.find_free_port.return_value=10001

        # when
        self.install_task.run()

        # then
        self.password_hasher_mock.random_password.assert_called_once_with()
        self.assertEqual(1, self.docker_client.create_container.call_count)

        for k in self.docker_client.create_container.call_args[1]:
            if k == 'environment':
                self.assertEqual({
                    'APPLICATION_ROOT': 'localhost:10001',
                    'AUTH_SOURCE_SECRET': 'generated_password'
                },self.docker_client.create_container.call_args[1][k])

    def test_has_content_to_update_after_install(self):
        """
        Ensure that the flag marking the application as having content to update
        is set to true after an application upgrade if the content version in the
        catalog was incremented.
        """
        app = Application(name="Test App", bundle="test.app", version="1.0.0")  # app without containers

        installed_app = InstalledApplication(bundle="test.app",
                                             current_state=ApplicationState.downloaded,
                                             target_state=ApplicationState.installed,
                                             token_endpoint_auth_method="client_secret_basic",
                                             target_version="1.0.0",
                                             has_content_to_upgrade=False)

        Content("c1", "Content", app, "down", "dest", "1.0.0")
        InstalledContent(
            content_id="c1",
            name="Content",
            installed_application=installed_app,
            download_path="down",
            destination_path="dest",
            current_state=ContentState.installed,
            target_state=ContentState.installed,
            target_version="0.0.1")

        self.application_repository.find_application_by_bundle.return_value = app
        self.application_repository.find_installed_application_by_bundle.return_value = installed_app

        # when
        self.install_task.run()

        # then
        self.assertTrue(installed_app.has_content_to_upgrade)

    def test_has_content_to_update_after_install_with_uninstalled_content(self):
        """
        Ensure that the flag marking the application as having content to update is set
        to false if the content was previously uninstalled
        """
        app = Application(name="Test App", bundle="test.app", version="1.0.0")  # app without containers

        installed_app = InstalledApplication(bundle="test.app",
                                             current_state=ApplicationState.downloaded,
                                             target_state=ApplicationState.installed,
                                             token_endpoint_auth_method="client_secret_basic",
                                             target_version="1.0.0",
                                             has_content_to_upgrade=False)

        Content("c1", "Content", app, "down", "dest", "1.0.0")
        InstalledContent("c1", "Content", installed_app, 1, "down", "dest", ContentState.uninstalled,
                 ContentState.uninstalled, "0.0.1")

        self.application_repository.find_application_by_bundle.return_value = app
        self.application_repository.find_installed_application_by_bundle.return_value = installed_app

        # when
        self.install_task.run()

        # then
        self.assertFalse(installed_app.has_content_to_upgrade)

    def test_env_var_set_for_config(self):
        """
        Ensure that environment variable are added to the container installation if the corresponding
        ConfigurationValue objects exists for that container
        """
        # given

        app = Application(name="Test App", bundle="test.app", version="1.0.0")  # app without containers

        installed_app = InstalledApplication(bundle="test.app",
                                             current_state=ApplicationState.downloaded,
                                             target_state=ApplicationState.installed,
                                             target_version="1.0.0")
        app.containers = [
            Container(image="ipfs:repo/image:latest", name="c1", application=app)
        ]

        InstalledContainer(name="test.app.c1",
                                installed_application=installed_app,
                                original_image="ipfs:repo/image:latest",
                                image="localhost:5000/repo/image:latest")
        ConfigurationValue(name="ENTRY", value="val", container="c1", installed_application=installed_app)

        self.application_repository.find_application_by_bundle.return_value = app
        self.application_repository.find_installed_application_by_bundle.return_value = installed_app
        self.free_port_finder.find_free_port.return_value=10001

        # when
        self.install_task.run()

        # then
        for k in self.docker_client.create_container.call_args[1]:
            if k == 'environment':
                self.assertEqual({
                    'ENTRY': 'val'
                },self.docker_client.create_container.call_args[1][k])

        self.application_repository.save.assert_has_calls([
            call(InstalledContainer(name="test.app.c1",
                                    installed_application=installed_app,
                                    host_port=None,
                                    original_image="ipfs:repo/image:latest",
                                    image=config.DOCKER_REGISTRY_HOST+"/ipfs:repo/image:latest"
            ))
        ])
