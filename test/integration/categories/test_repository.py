from app.applications.models import InstalledApplication, InstalledContent, ContentState
from app.categories.repository import CategoryRepository
from app.users.models import User
from test.integration.abstract_integration_test import AbstractIntegrationTest

from app.categories.models import Term, Category, CategoryLabel, Playlist


class CategoryRepositoryTestCase(AbstractIntegrationTest):
    """
    Tests for the categories repository
    """

    def setUp(self):
        self.category_repository = CategoryRepositoryTestCase.flask_injector.injector.get(CategoryRepository)

    def test_delete_all_non_custom_terms(self):
        t1 = Term("term1", custom=False)
        t2 = Term("term2", custom=False)
        t3 = Term("term3", custom=True)

        CategoryRepositoryTestCase.db.session.add(t1)
        CategoryRepositoryTestCase.db.session.add(t2)
        CategoryRepositoryTestCase.db.session.add(t3)

        self.category_repository.delete_all_non_custom_terms()

        all_remaining_terms = CategoryRepositoryTestCase.db.session.query(Term).all()

        self.assertEqual([t3], all_remaining_terms)

    def test_get_all_terms(self):
        t1 = Term("term1", custom=False)
        t2 = Term("term2", custom=True)

        CategoryRepositoryTestCase.db.session.add(t1)
        CategoryRepositoryTestCase.db.session.add(t2)

        all_terms = self.category_repository.get_all_terms()

        self.assertEqual([t1, t2], all_terms )

    def test_get_all_categories(self):
        c1 = Category()
        c2 = Category()

        CategoryRepositoryTestCase.db.session.add(c1)
        CategoryRepositoryTestCase.db.session.add(c2)

        all_cats = self.category_repository.get_all_categories()

        self.assertEqual([c1, c2], all_cats)

    def test_get_category_by_id(self):
        c1 = Category()
        c2 = Category()

        CategoryRepositoryTestCase.db.session.add(c1)
        CategoryRepositoryTestCase.db.session.add(c2)
        CategoryRepositoryTestCase.db.session.flush()

        cat = self.category_repository.get_category_by_id(c2.id)

        self.assertEqual(c2, cat)

    def test_get_term_by_id(self):
        t1 = Term(id=1, term="term1", custom=False)
        t2 = Term(id=2, term="term2", custom=False)

        CategoryRepositoryTestCase.db.session.add(t1)
        CategoryRepositoryTestCase.db.session.add(t2)
        CategoryRepositoryTestCase.db.session.flush()

        term = self.category_repository.get_term_by_id(1)
        self.assertEqual(t1, term)

    def test_get_all_playlists(self):
        u = User(name="name", username="username", password="123")
        c1 = Playlist(title="title1", user=u)
        c2 = Playlist(title="title2", user=u)

        CategoryRepositoryTestCase.db.session.add(u)
        CategoryRepositoryTestCase.db.session.add(c1)
        CategoryRepositoryTestCase.db.session.add(c2)

        all_cats = self.category_repository.get_all_playlists()

        self.assertEqual([c1, c2], all_cats)

    def test_get_playlist_by_id(self):
        u = User(name="name", username="username", password="123")
        c1 = Playlist(title="title1", user=u)
        c2 = Playlist(title="title2", user=u)

        CategoryRepositoryTestCase.db.session.add(c1)
        CategoryRepositoryTestCase.db.session.add(c2)
        CategoryRepositoryTestCase.db.session.flush()

        cat = self.category_repository.get_playlist_by_id(c2.id)

        self.assertEqual(c2, cat)
