import config

from unittest import TestCase
from unittest.mock import Mock

from app.applications import ApplicationRepository
from app.applications.models import InstalledApplication, InstalledContent, ContentState, Application, Content
from app.applications.state_machine.uninstall_content_task import UninstallContentTask
from app.applications.storage.storage import Storage
from app.applications.storage.storage_manager import StorageManager

class UninstallContentTaskTest(TestCase):

    def setUp(self):
        self.application_repository_mock = Mock(spec=ApplicationRepository)

        self.storage_manager_mock = Mock(spec=StorageManager)
        self.storage_mock = Mock(spec=Storage)
        self.storage_manager_mock.storage_for_url.return_value = self.storage_mock

        self.uninstall_content_task = UninstallContentTask(self.application_repository_mock, config,
                                                       self.storage_manager_mock)

    def test_uninstall_content(self):
        # given
        config.HOST_DATA_DIR="/tmp"
        self.uninstall_content_task.set_content(bundle_id="bundle.id", content_id="content.id")
        app = Application(name="Test App", bundle="bundle.id", version="1.0.0")  # app without containers
        Content("content.id", "Content", app, "down", "dest", "1.0.0")

        ia = InstalledApplication(bundle="bundle.id", target_version="1.0.0", has_content_to_upgrade=True)
        ic = InstalledContent(content_id="content.id", installed_application=ia, name="Content",
                              size=1, download_path="ipfs:/ipfs/path", destination_path="a/folder/path",
                              current_state=ContentState.installed, target_state=ContentState.installed,
                              target_version="1.0.0")

        self.application_repository_mock.find_installed_application_by_bundle.return_value = ia
        self.application_repository_mock.find_application_by_bundle.return_value = app

        # when
        self.uninstall_content_task.run()

        # then
        self.storage_manager_mock.storage_for_url.assert_called_with('ipfs:/ipfs/path')
        self.storage_mock.remove_content.assert_called_with('ipfs:/ipfs/path',
            'a/folder/path',
            '/tmp/bundle.id/content'
        )
        self.assertEqual(ContentState.uninstalled, ic.current_state)
        self.application_repository_mock.commit.assert_called_once_with()
        self.assertFalse(ia.has_content_to_upgrade)


