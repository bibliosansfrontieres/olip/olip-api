from app.core.password_hasher import PasswordHasher
from app.users.repository import UserRepository
from app.users.service import UserService
from .resources import user_api
from injector import Module, Key, singleton

UserApi = Key('UserApi')


class UserModule(Module):
    def configure(self, binder):
        binder.bind(UserRepository, to=UserRepository, scope=singleton)
        binder.bind(UserApi, to=user_api)
        binder.bind(PasswordHasher, to=PasswordHasher, scope=singleton)
        binder.bind(UserService, to=UserService, scope=singleton)


