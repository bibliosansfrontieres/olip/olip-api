from flask_sqlalchemy import SQLAlchemy
from injector import inject
from app.logo.models import Logo, logo


# from app.logo.models import (Logo)


class LogoRepository:

    """Persistence of logo"""
    @inject
    def __init__(self, db: SQLAlchemy):
        self._db = db

    def get_logo(self, id):
        return self._db.session.query(Logo).filter(Logo.id == id).first()

    def save(self, object):
        self._db.session.add(object)

    def delete(self, object):
        self._db.session.delete(object)

    def flush(self):
        self._db.session.flush()
