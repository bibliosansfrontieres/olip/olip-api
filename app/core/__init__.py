from app.applications import ApplicationModule
from app.core.module import CoreModule
from app.core.scheduler import SchedulerModule
from app.platform import PlatformModule
from app.search import SearchModule
from app.users import UserModule
from app.oidc.oidc import OIDCServerModule
from .module import configure
from .db import DatabaseModule, MigrateModule
from .keys import *


all_modules = [configure, CoreModule, DatabaseModule, MigrateModule, ApplicationModule, UserModule, SchedulerModule,
               OIDCServerModule, PlatformModule, SearchModule]