import pathlib

from urllib import parse

from injector import inject

from app.applications.storage.storage import FileBasedStorage
from app.core.filesystem import Filesystem
import os
import json


class LocalStorage (FileBasedStorage):

    @inject
    def __init__(self, filesystem: Filesystem):
        self.filesystem = filesystem

    def get_protocol(self):
        return ["file"]

    def _get_path_from_url(self, path):
        without_prefix = self.remove_prefix(path)

        # remove double leading slashes as well
        return without_prefix[2:]

    def download_descriptor(self, path):
        # remove double leading slashes as well
        without_slashes = self._get_path_from_url(path)

        full_path = os.path.join(without_slashes, "descriptor.json")

        content = self.filesystem.read_fully_as_string(full_path)

        return json.JSONDecoder().decode(content)

    def download_descriptor_resource(self, path, resource):
        # remove double leading slashes as well
        without_slashes = self._get_path_from_url(path)

        full_path = os.path.join(without_slashes, resource)

        content = self.filesystem.read_fully(full_path)

        return content

    def download_docker_image(self, image):
        # Nothing to do there, we will simply rely on a docker registry pull
        pass

    def delete_docker_image(self, image):
        # Same
        pass

    def _unzip_into_folder(self, content, dest_path):
        src_path = self._get_path_from_url(content)

        self.filesystem.unzipInto(src_path, dest_path)

    def _copy_into_folder(self, content, dest_path):
        src_path = self._get_path_from_url(content)

        self.filesystem.cp(src_path, dest_path)



