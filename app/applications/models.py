import enum

from sqlalchemy import Table, Column, Integer, String, Text, ForeignKey, Enum, Index, Boolean
from sqlalchemy.orm import mapper, relationship
from app.core.db import metadata

from typing import List

class ApplicationState(enum.Enum):
    uninstalled = "uninstalled"
    downloaded = "downloaded"
    installed = "installed"


class ContentState(enum.Enum):
    uninstalled = "uninstalled"
    installed = "installed"
    broken = "broken"


application = Table('applications', metadata,
                      Column('id', Integer, primary_key=True, autoincrement=True),
                      Column('bundle',String(50), nullable=False, index=True, unique=True),
                      Column('name',String(50), nullable=False),
                      Column('version', String(length=20), nullable=False),
                      Column('description', Text(length=32000)),
                      Column('picture', Text(length=32000)),
                      Column('grant_types', String(120)),
                      Column('response_types', String(120)),
                      Column('token_endpoint_auth_method', String(20)),
                      Column('search_container', String),
                      Column('search_url', String),
                      Column('auth_source_container', String),
                      Column('auth_source_url', String)
                    )

content = Table('contents', metadata,
                    Column('id', Integer, primary_key=True, autoincrement=True),
                    Column('content_id', String(50)),
                    Column('name', String(50)),
                    Column('description', String(250)),
                    Column('language', String(10)),
                    Column('subject', String(50)),
                    Column('application_id', Integer, ForeignKey('applications.id'), nullable=False),
                    Column('download_path', String(255), nullable=False),
                    Column('destination_path', String(255), nullable=False),
                    Column('version', String(20)),
                    Column('size', Integer, nullable=True),
                    Column('endpoint_container', String(20)),
                    Column('endpoint_name', String(50)),
                    Column('endpoint_url', String(255))
                )

container = Table('containers', metadata,
                    Column('id', Integer, primary_key=True, autoincrement=True),
                    Column('application_id', Integer, ForeignKey('applications.id'), nullable=False),
                    Column('image', String(100), nullable=False),
                    Column('name', String(20)),
                    Column('expose', Integer)
                  )

configuration = Table('configuration', metadata,
                  Column('id', Integer, primary_key=True, autoincrement=True),
                  Column('container_id', Integer, ForeignKey('containers.id'), nullable=False),
                  Column('name', String(50), nullable=False),
                  Column('description', String(255))
                  )

installed_applications = Table('installed_applications', metadata,
                                Column('id', Integer, primary_key=True, autoincrement=True),
                                Column('bundle', String(50), nullable=False),
                                Column('target_state', Enum(ApplicationState), nullable=False),
                                Column('current_state', Enum(ApplicationState), nullable=False),
                                Column('client_secret', String(50)),
                                Column('grant_types', String(120)),
                                Column('response_types', String(120)),
                                Column('token_endpoint_auth_method', String(20)),
                                Column('current_version', String(20)),
                                Column('target_version', String(20)),
                                Column('has_content_to_upgrade', Boolean),
                                Column('visible', Boolean),
                                Column('display_weight', Integer),
                                Column('search_container', String),
                                Column('search_url', String),
                                Column('auth_source_container', String),
                                Column('auth_source_url', String),
                                Column('auth_source_secret', String)
                               )

installed_container = Table('installed_containers', metadata,
                            Column('id', Integer, primary_key=True, autoincrement=True),
                            Column('installed_application_id', Integer, ForeignKey('installed_applications.id'), nullable=False),
                            Column('name', String(20), unique=True),
                            Column('host_port', Integer, unique=True),
                            Column('original_image', String(100)),
                            Column('image', String(100)),
                            )

configuration_values = Table('configuration_values', metadata,
                      Column('id', Integer, primary_key=True, autoincrement=True),
                      Column('installed_application_id', Integer, ForeignKey('installed_applications.id'), nullable=False),
                      Column('container', String(20), nullable=False),
                      Column('name', String(50), nullable=False),
                      Column('value', String(255)),
                      Column('description', String(255))
                      )

installed_content = Table('installed_contents', metadata,
                            Column('id', Integer, primary_key=True, autoincrement=True),
                            Column('content_id', String(50)),
                            Column('name', String(50)),
                            Column('size', Integer, nullable=True),
                            Column('language', String(10)),
                            Column('subject', String(50)),
                            Column('installed_application_id', Integer, ForeignKey('installed_applications.id'), nullable=False),
                            Column('download_path', String(255), nullable=False),
                            Column('destination_path', String(255), nullable=False),
                            Column('current_state', Enum(ContentState), nullable=False),
                            Column('target_state', Enum(ContentState), nullable=False),
                            Column('current_version', String(20)),
                            Column('target_version', String(20)),
                            Column('endpoint_container', String(20)),
                            Column('endpoint_name', String(50)),
                            Column('endpoint_url', String(255))
                          )



# place an index on col3, col4
Index('idx_app_id_content_id', content.c.application_id, content.c.content_id, unique=True)
Index('idx_inst_app_id_content_id', installed_content.c.installed_application_id, installed_content.c.content_id, unique=True)
Index('idx_inst_app_display_weight', installed_applications.c.display_weight)
Index('idx_inst_app_search_container', installed_applications.c.search_container)
Index('idx_inst_app_auth_source_container', installed_applications.c.auth_source_container)


class Configuration(object):
    def __init__(self, container, name, description=None, id=None):
        self.id = id
        self.container = container
        self.name = name
        self.description = description

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Configuration):
            return False

        eq = self.id == other.id \
             and self.container == other.container \
             and self.name == other.name \
             and self.description == other.description

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.container ,self.name, self.description))

class Container(object):

    def __init__(self, application, image, name, expose=None):
        self.id = None
        self.application = application
        self.image = image
        self.name = name
        self.expose = expose

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Container):
            return False

        eq = self.id == other.id \
                and self.image == other.image \
                and self.application == other.application \
                and self.name == other.name \
                and self.expose == other.expose

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.application ,self.image, self.name, self.expose))


class Application(object):

    def __init__(self, name, bundle, version, description=None, picture=None, id=None, grant_types=None,
                 response_types=None, token_endpoint_auth_method=None, search_container=None, search_url=None,
                 auth_source_container=None, auth_source_url=None):
        self.name = name
        self.bundle = bundle
        self.version = version
        self.description = description
        self.picture = picture
        self.id = id
        self.grant_types = grant_types
        self.response_types = response_types
        self.token_endpoint_auth_method = token_endpoint_auth_method
        self.search_container = search_container
        self.search_url = search_url
        self.auth_source_container = auth_source_container
        self.auth_source_url = auth_source_url

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Application):
            return False

        eq = self.id == other.id \
                and self.name == other.name \
                and self.bundle == other.bundle \
                and self.version == other.version \
                and self.description == other.description \
                and self.picture == other.picture \
                and self.grant_types == other.grant_types \
                and self.response_types == other.response_types \
                and self.token_endpoint_auth_method == other.token_endpoint_auth_method \
                and self.search_container == other.search_container \
                and self.search_url == other.search_url \
                and self.auth_source_container == other.auth_source_container \
                and self.auth_source_url == other.auth_source_url


        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.name, self.bundle, self.version, self.description, self.picture,
                     self.grant_types, self.response_types, self.token_endpoint_auth_method,
                     self.search_container, self.search_url, self.auth_source_container, self.auth_source_url))


class Content(object):
    def __init__(self, content_id, name, application, download_path, destination_path, version, size=0, description=None,
                 language=None, subject=None, id=None, endpoint_container=None, endpoint_name=None, endpoint_url=None):
        self.id = None
        self.content_id=content_id
        self.name = name
        self.application = application
        self.download_path = download_path
        self.destination_path = destination_path
        self.description = description
        self.language = language
        self.subject = subject
        self.version = version
        self.size = size
        self.endpoint_container = endpoint_container
        self.endpoint_name = endpoint_name
        self.endpoint_url = endpoint_url
        self.id = id

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Content):
            return False

        eq = self.id == other.id \
                and self.content_id == other.content_id \
                and self.name == other.name \
                and self.application == other.application \
                and self.download_path == other.download_path \
                and self.destination_path == other.destination_path \
                and self.description == other.description \
                and self.language == other.language \
                and self.subject == other.subject \
                and self.version == other.version \
                and self.size == other.size \
                and self.endpoint_container == other.endpoint_container \
                and self.endpoint_name == other.endpoint_name \
                and self.endpoint_url == other.endpoint_url

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.content_id, self.name, self.application ,self.download_path,
                     self.destination_path, self.description, self.language, self.subject, self.size, self.version,
                     self.endpoint_container, self.endpoint_name, self.endpoint_url))


class InstalledApplication(object):

    def __init__(self, bundle, target_version, current_version=None, target_state=ApplicationState.uninstalled,
                 current_state=ApplicationState.uninstalled, id=None, client_secret=None, grant_types=None,
                 response_types=None, token_endpoint_auth_method=None, has_content_to_upgrade=False, visible=True,
                 display_weight=0, search_container=None, search_url=None, auth_source_container=None,
                 auth_source_url=None, auth_source_secret=None):

        self.id = id
        self.bundle = bundle
        self.current_state = current_state
        self.target_state = target_state
        self.client_secret = client_secret
        self.grant_types = grant_types
        self.response_types = response_types
        self.token_endpoint_auth_method = token_endpoint_auth_method
        self.current_version = current_version
        self.target_version = target_version
        self.has_content_to_upgrade = has_content_to_upgrade
        self.visible = visible
        self.display_weight = display_weight
        self.search_container = search_container
        self.search_url = search_url
        self.auth_source_container = auth_source_container
        self.auth_source_url = auth_source_url
        self.auth_source_secret = auth_source_secret

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, InstalledApplication):
            return False

        eq = self.id == other.id \
            and self.bundle == other.bundle \
            and self.current_state == other.current_state \
            and self.target_state == other.target_state \
            and self.client_secret == other.client_secret \
            and self.grant_types == other.grant_types \
            and self.response_types == other.response_types \
            and self.token_endpoint_auth_method == other.token_endpoint_auth_method \
            and self.current_version == other.current_version \
            and self.target_version == other.target_version \
            and self.has_content_to_upgrade == other.has_content_to_upgrade \
            and self.visible == other.visible \
            and self.display_weight == other.display_weight \
            and self.search_container == other.search_container \
            and self.search_url == other.search_url \
            and self.auth_source_container == other.auth_source_container \
            and self.auth_source_url == other.auth_source_url \
            and self.auth_source_secret == other.auth_source_secret

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.bundle, self.current_state, self.client_secret, self.grant_types,
                     self.response_types, self.token_endpoint_auth_method, self.current_version, self.target_version,
                     self.has_content_to_upgrade, self.visible, self.display_weight, self.target_state,
                     self.search_container, self.search_url, self.auth_source_container, self.auth_source_url,
                     self.auth_source_secret))


class ConfigurationValue(object):
    def __init__(self, installed_application, container, name, value = None, description=None, id=None):
        self.id = id
        self.installed_application = installed_application
        self.container = container
        self.name = name
        self.value = value
        self.description = description

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, ConfigurationValue):
            return False

        eq = self.id == other.id \
             and self.installed_application == other.installed_application \
             and self.container == other.container \
             and self.name == other.name \
             and self.value == other.value \
             and self.description == other.description

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.container, self.installed_application, self.name, self.description))


class InstalledContainer(object):

    def __init__(self, name, installed_application, original_image, image, host_port=None, id=None):
        self.id = id
        self.name = name
        self.host_port = host_port
        self.original_image=original_image
        self.image = image
        self.installed_application = installed_application

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, InstalledContainer):
            return False

        eq = self.id == other.id \
             and self.name == other.name \
             and self.host_port == other.host_port \
             and self.original_image == other.original_image \
             and self.image == other.image \
             and self.installed_application == other.installed_application

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.name, self.host_port, self.original_image, self.image, self.installed_application))


class InstalledContent(object):
    def __init__(self, content_id, name, installed_application, download_path, destination_path, current_state,
                 target_state, target_version, language=None, subject=None, size=0, current_version=None, id=None,
                 endpoint_container=None, endpoint_name=None, endpoint_url=None):
        self.id = id
        self.content_id = content_id
        self.name = name
        self.size = size
        self.installed_application = installed_application
        self.download_path = download_path
        self.destination_path = destination_path
        self.current_state = current_state
        self.target_state = target_state
        self.language = language
        self.subject = subject
        self.current_version = current_version
        self.target_version = target_version
        self.endpoint_container = endpoint_container
        self.endpoint_name = endpoint_name
        self.endpoint_url = endpoint_url

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, InstalledContent):
            return False

        eq = self.id == other.id \
             and self.content_id == other.content_id \
             and self.name == other.name \
             and self.size == other.size \
             and self.installed_application == other.installed_application \
             and self.download_path == other.download_path \
             and self.destination_path == other.destination_path \
             and self.current_state == other.current_state \
             and self.target_state == other.target_state \
             and self.language == other.language \
             and self.subject == other.subject \
             and self.current_version == other.current_version \
             and self.target_version == other.target_version \
             and self.endpoint_container == other.endpoint_container \
             and self.endpoint_name == other.endpoint_name \
             and self.endpoint_url == other.endpoint_url

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.content_id, self.name, self.size,
                     self.installed_application, self.destination_path, self.current_state, self.target_state,
                     self.language, self.subject, self.current_version, self.target_version, self.endpoint_url,
                     self.endpoint_name, self.endpoint_container))


mapper(Application, application, properties={
    'containers': relationship(Container, back_populates="application", cascade="all, delete-orphan"),
    'contents': relationship(Content, back_populates="application", cascade="all, delete-orphan")
})

mapper(Container, container, properties={
    'application': relationship(Application, back_populates="containers"),
    'configuration': relationship(Configuration, back_populates="container", cascade="all, delete-orphan")
})

mapper(Configuration, configuration, properties={
    'container': relationship(Container, back_populates="configuration")
})

mapper(Content, content, properties={
    'application': relationship(Application, back_populates="contents")
})

mapper(InstalledApplication, installed_applications, properties={
    'installed_containers': relationship(InstalledContainer, back_populates="installed_application", cascade="all, delete-orphan"),
    'installed_contents': relationship(InstalledContent, back_populates="installed_application", cascade="all, delete-orphan"),
    'configuration_values': relationship(ConfigurationValue, back_populates="installed_application", cascade="all, delete-orphan")
})

mapper(InstalledContainer, installed_container, properties={
    'installed_application': relationship(InstalledApplication, back_populates="installed_containers"),

})

mapper(ConfigurationValue, configuration_values, properties={
    'installed_application': relationship(InstalledApplication, back_populates="configuration_values")
})

mapper(InstalledContent, installed_content, properties={
    'installed_application': relationship(InstalledApplication, back_populates="installed_contents")
})


# ####### TRANSIENT MODELS ##########
# These models are not mapped to a table

class EndpointType(enum.Enum):
    application="application"
    content="content"


class Endpoint:
    def __init__(self, name, url, type):
        self.name = name
        self.url = url
        self.type = type

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.name == other.name \
            and self.url == other.url \
            and self.type == other.type

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.name, self.url, self.type))


class ConfigParameter:
    def __init__(self, name: String, value: String = None, description:String = None):
        self.name = name
        self.value = value
        self.description = description

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.name == other.name \
             and self.value == other.value \
             and self.description == other.description

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.name, self.value, self.description))


class ContainerConfig:

    def __init__(self, container, parameters: List[ConfigParameter]):
        self.container = container
        self.parameters = parameters

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.container == other.container \
             and self.parameters == other.parameters

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.container, self.parameters))
