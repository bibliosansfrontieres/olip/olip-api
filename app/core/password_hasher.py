import random, string

import bcrypt

class PasswordHasher:

    def hash(self, password):

        encoded_pass = password.encode("utf-8")

        return bcrypt.hashpw(encoded_pass, bcrypt.gensalt())

    def verify(self, clear_text, hashed):
        return bcrypt.checkpw(clear_text.encode("utf-8"), hashed)

    def random_password(self, length=12):

        myrg = random.SystemRandom()

        alphabet = string.ascii_letters + string.digits + string.punctuation

        pw = str().join(myrg.choice(alphabet) for _ in range(length))

        return pw

