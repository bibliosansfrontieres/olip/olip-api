from app.applications.state_machine.delete_task import DeleteTask
from app.applications.state_machine.download_task import DownloadTask
from app.applications.state_machine.install_task import InstallTask, FreePortFinder
from app.applications.state_machine.uninstall_task import UninstallTask
from .state_machine.state_machine import StateMachine
from .resources import application_api
from injector import Module, Key, singleton
from .repository import ApplicationRepository
from .resources import ApplicationResource
from .service import ApplicationService

ApplicationApi = Key('ApplicationApi')


class ApplicationModule(Module):
    def configure(self, binder):
        binder.bind(ApplicationResource, to=ApplicationResource)
        binder.bind(ApplicationRepository, to=ApplicationRepository, scope=singleton)
        binder.bind(ApplicationApi, to=application_api)
        binder.bind(StateMachine, to=StateMachine, scope=singleton)
        binder.bind(DownloadTask, to=DownloadTask)
        binder.bind(InstallTask, to=InstallTask)
        binder.bind(FreePortFinder, to=FreePortFinder)
        binder.bind(ApplicationService, to=ApplicationService, scope=singleton)
        binder.bind(UninstallTask, to=UninstallTask)
        binder.bind(DeleteTask, to=DeleteTask)


