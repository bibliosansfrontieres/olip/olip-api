# OLIP API
[![pipeline status](https://gitlab.com/bibliosansfrontieres/olip/olip-api/badges/master/pipeline.svg)](https://gitlab.com/bibliosansfrontieres/olip/olip-api/commits/master)

## Virtual Environment
In order to create a local virtual environment you need to use `setuptools`<=58.
More recent versions break support for `use_2to3` and some packages in the list
won't build.  
If your operating system, distribution or package manager is recent and up to
date, chances are that your version of setuptools will break.  
The recommended way is to use [pipenv](https://github.com/pypa/pipenv) thusly:  
When inside the `olip-api` repository, run: `pipenv install setuptools==58`  
That should start by replacing the stock setuptools originally seeded by
pipenv and then it will go on and install the rest of the requirements.txt.  
The proper thing to do will be to replace the requirements.txt by a pipfile.
But that will require to rewrite the dockerfile, that will be done at a later
stage. Thank you for using this workaround meanwhile.
