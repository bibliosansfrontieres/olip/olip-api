from sqlalchemy import (Table, Column, Integer, String, Boolean,
                        Index, ForeignKey, LargeBinary, UniqueConstraint)

from app.core.db import metadata
from sqlalchemy.orm import mapper


logo = Table('logo', metadata,
             Column('id', Integer, primary_key=True, autoincrement=True),
             Column('thumbnail_mime_type', String(255)),
             Column('thumbnail', LargeBinary)
             )


class Logo(object):

    def __init__(self, thumbnail=None, thumbnail_mime_type=None, id=None):
        self.id = id
        self.thumbnail = thumbnail
        self.thumbnail_mime_type = thumbnail_mime_type

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id and \
            self.thumbnail == other.thumbnail and \
            self.thumbnail_mime_type == other.thumbnail_mime_type

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.thumbnail, self.thumbnail_mime_type))


mapper(Logo, logo)
