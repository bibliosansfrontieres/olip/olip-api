from unittest import TestCase
from unittest.mock import Mock

from flask import Flask
from flask_injector import FlaskInjector
from flask_restplus import Api
from injector import Module, provider, singleton

from app import config
from app.users import user_api, UserService, UserModule
from app.users.models import User
from test.integration.applications.test_resources import configure_config

class ServiceMockModule(Module):
    def __init__(self, mock):
        self.mock = mock

    @provider
    @singleton
    def provide_mock(self) -> UserService:
        return self.mock


class UserResourcesTest(TestCase):

    def setUp(self):
        app = Flask(__name__, instance_relative_config=True)
        # Load the config file
        app.config.from_object(config)
        config.DISABLE_AUTH = True

        # Initiate the Flask API
        api = Api(
            title='Box API',
            version='1.0',
            description='Box API',
            # All API metadatas
        )

        # Load application namespace
        api.add_namespace(user_api, path="/users")

        # Bootstrap app
        api.init_app(app)

        self.service_mock=Mock(spec=UserService)
        modules = [configure_config, UserModule, ServiceMockModule(self.service_mock)]
        self.injector = FlaskInjector(app=app, modules=modules)

        self.client = app.test_client()

    def test_get_user_list(self):
        # given
        users = [
            User(username="john.doe", name="John Doe", admin=True, password="==1234=="),
            User(username="jane.doe", name="Jane Doe", provider="test.app")
        ]

        self.service_mock.get_all.return_value = users

        # when
        response = self.client.get("/users/")

        # then
        self.assertEqual(response.status_code, 200)

        self.service_mock.get_all.assert_called_once_with()

        self.assertEqual({'data': [{'username': 'john.doe',
                                    'name': 'John Doe',
                                    'admin': True,
                                    'links': [
                                        {
                                            'rel': 'self',
                                            'href': 'http://localhost/users/john.doe'
                                        }
                                    ]},
                                   {'username': 'jane.doe',
                                    'provider': 'test.app',
                                    'name': 'Jane Doe',
                                    'admin': False,
                                    'links': [
                                        {
                                            'rel': 'self',
                                            'href': 'http://localhost/users/test.app|jane.doe'
                                        }
                                    ]}]
                          },
                         response.json)

    def test_get_user(self):
        # given
        user = User(username="john.doe", name="John Doe", password="==1234==")

        self.service_mock.get_by_username_and_provider.return_value = user

        # when
        response = self.client.get("/users/john.doe")

        # then
        self.assertEqual(response.status_code, 200)

        self.service_mock.get_by_username_and_provider.assert_called_once_with("john.doe", provider=None)

        self.assertEqual({'username': 'john.doe',
                          'name': 'John Doe',
                          'admin': False,
                          'links': [
                            {
                              'rel': 'self',
                              'href': 'http://localhost/users/john.doe'
                            }
                           ]
                          }, response.json)

    def test_get_user_with_external_provider(self):
        # given
        user = User(username="john.doe", name="John Doe", provider="test.app")

        self.service_mock.get_by_username_and_provider.return_value = user

        # when
        response = self.client.get("/users/test.app|john.doe")

        # then
        self.assertEqual(response.status_code, 200)

        self.service_mock.get_by_username_and_provider.assert_called_once_with("john.doe", provider="test.app")

        self.assertEqual({'provider': 'test.app',
                          'username': 'john.doe',
                          'name': 'John Doe',
                          'admin': False,
                          'links': [
                              {
                                  'rel': 'self',
                                  'href': 'http://localhost/users/test.app|john.doe'
                              }
                          ]
                          }, response.json)

    def test_create_user(self):
        # given
        user = User(username="john.doe", name="John Doe", admin=True, password="==1234==")
        self.service_mock.create_or_update_user.return_value = user

        # when
        response = self.client.put("/users/john.doe", data={
            "name": "John Doe",
            "admin": True,
            "password": "123123"
        })

        # then
        self.assertEqual(response.status_code, 200)
        self.assertEqual({
            'username': 'john.doe',
            'name': 'John Doe',
            'admin': True,
            'links': [
                {
                    'rel': 'self',
                    'href': 'http://localhost/users/john.doe'
                }
            ]
        }, response.json)
        self.service_mock.create_or_update_user.assert_called_once_with("john.doe", "John Doe", True, "123123")

    def test_create_user_with_forbidden_caracter(self):
        # given
        self.service_mock.create_or_update_user.return_value = None

        # when
        response = self.client.put("/users/john|doe", data={
            "name": "John Doe",
            "password": "123123"
        })

        # then
        self.assertEqual(response.status_code, 400)
        self.assertEqual({
            'message': 'Incorrect user name'
        }, response.json)
        self.service_mock.create_or_update_user.assert_not_called()




