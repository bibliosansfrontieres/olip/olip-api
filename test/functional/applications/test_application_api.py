import json

import config
from app.applications.models import Application, Container, InstalledApplication, InstalledContainer, Content, \
    ConfigurationValue
from test.functional.abstract_functional_test import AbstractFunctionalTest


class ApplicationApiTest(AbstractFunctionalTest):

    def test_get_application(self):
        existing_app = Application(name="testapp2", bundle="test.app.2", version="1.0.0")
        ApplicationApiTest.db.session.add(existing_app)
        ApplicationApiTest.db.session.flush()

        response = self.client.get('/applications/test.app.2')

        json = response.json

        self.assertEqual(200, response.status_code)
        self.assertEqual('test.app.2', json['bundle'])

    def test_get_application_picture(self):
        existing_app = Application(name="testapp2", bundle="test.app.2", version="1.0.0", picture="AQID")
        ApplicationApiTest.db.session.add(existing_app)
        ApplicationApiTest.db.session.flush()

        response = self.client.get('/applications/test.app.2/picture')

        data = response.data

        self.assertEqual(200, response.status_code)
        self.assertEqual(bytearray([0x01,0x02,0x03]), data)

    def test_get_application_contents(self):
        existing_app = Application(name="testapp2", bundle="test.app.2", version="1.0.0", picture="AQID")
        installed_app = InstalledApplication(bundle="test.app.2", target_version="1.0.0")
        content = Content(name="content 1", content_id="c1", application=existing_app,
                          download_path="/ipfs/ABCDE", destination_path="filename", version="1.0.0")
        ApplicationApiTest.db.session.add(existing_app)
        ApplicationApiTest.db.session.add(installed_app)
        ApplicationApiTest.db.session.add(content)
        ApplicationApiTest.db.session.commit()

        response = self.client.get('/applications/test.app.2/contents')

        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(response.json['data']))
        self.assertEqual("c1", response.json['data'][0]['content_id'])

    def test_set_get_application_content_target_state(self):
        existing_app = Application(name="testapp2", bundle="test.app.2", version="1.0.0", picture="AQID")
        installed_application = InstalledApplication(bundle="test.app.2", target_version="1.0.0")
        content = Content(name="content 2", content_id="c2", application=existing_app,
                          download_path="/ipfs/ABCDE", destination_path="filename", version="1.0.0")
        ApplicationApiTest.db.session.add(existing_app)
        ApplicationApiTest.db.session.add(installed_application)
        ApplicationApiTest.db.session.add(content)
        ApplicationApiTest.db.session.commit()

        response = self.client.put('/applications/test.app.2/contents/c2/target-state', data= {
            'target_state': 'installed'
        })

        self.assertEqual(200, response.status_code)

        response = self.client.get('/applications/test.app.2/contents/c2/target-state')

        self.assertEqual(200, response.status_code)

        self.assertEqual("installed", response.json['target_state'])

    def test_get_set_configuration(self):
        existing_app = Application(name="testapp2", bundle="test.app.2", version="1.0.0", picture="AQID")
        installed_application = InstalledApplication(bundle="test.app.2", target_version="1.0.0")
        installed_container = InstalledContainer("test.app.2.c1", installed_application=installed_application,
                                                 image="test/img", original_image="ipfs:test/img")
        config_value = ConfigurationValue(name="entry",
                                          description="entry description",
                                          value="v1",
                                          container="c1",
                                          installed_application=installed_application)
        ApplicationApiTest.db.session.add(existing_app)
        ApplicationApiTest.db.session.add(installed_application)
        ApplicationApiTest.db.session.add(installed_container)
        ApplicationApiTest.db.session.add(config_value)
        ApplicationApiTest.db.session.commit()

        self.client.put('/applications/test.app.2/configuration', data= json.dumps({
            'configuration': [
                {
                    'container': 'c1',
                    'parameters': [
                        {
                            'name': 'entry',
                            'value': 'v2'
                        }
                    ]
                }
            ]
        }), content_type='application/json')

        response = self.client.get('/applications/test.app.2/configuration')

        self.assertEqual({
            'configuration': [
                {
                    'container': 'c1',
                    'parameters': [
                        {
                            'name': 'entry',
                            'description': 'entry description',
                            'value': 'v2'
                        }
                    ]
                }
            ]
        }, response.json)

