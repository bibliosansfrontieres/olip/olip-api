"""add_weight__to_playlists_and_categories

Revision ID: 333435b6522d
Revises: 74903cdb0ae6
Create Date: 2021-06-17 10:24:39.711023

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '333435b6522d'
down_revision = '74903cdb0ae6'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('playlists', sa.Column(
        'display_weight', sa.Integer(), nullable=True))
    op.add_column('categories', sa.Column(
        'display_weight', sa.Integer(), nullable=True))


def downgrade():
    op.drop_column('playlists', 'display_weight')
    op.drop_column('categories', 'display_weight')
