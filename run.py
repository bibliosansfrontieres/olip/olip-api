import os

from app.core.module import app
from app import flask_injector, Scheduler, UserService

import logging
import sys
import config

from app.users import UserRepository

logger = logging.getLogger("run")
if __name__ == '__main__':

    root = logging.getLogger()
    root.setLevel(config.DEBUG)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)

    logger.info("Starting app")

    werkzeug = os.environ.get('WERKZEUG_RUN_MAIN')
    if (not werkzeug) or (werkzeug == 'false'):
        flask_injector.injector.get(Scheduler)

    # trigger user creation if no user is found
    user_service = flask_injector.injector.get(UserService)

    if user_service.create_admin_user_if_no_user_exists():
        flask_injector.injector.get(UserRepository).commit()

    app.run(host="0.0.0.0", port=os.getenv('PORT', "5002"))

