from unittest import TestCase
from unittest.mock import Mock

import asynctest
import httpretty
import asyncio
import async_timeout

from mocket import mocketize
from mocket.mockhttp import Entry

from app.search.models import SearchResult
from app.search.opensearch_client import OpenSearchClient, OpenSearchUrlTokenizer

opensearch_descriptor = """<?xml version="1.0" encoding="UTF-8"?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
  <ShortName>Third party app</ShortName>
  <Description>Third party app search engine</Description>
  <Url type="application/atom+xml" template="http://localhost:9991/opensearch/search?q={searchTerms}"/>
</OpenSearchDescription>
"""

atom_feed = """<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>Olip search results</title>
  <link href="http://localhost/opensearch/search?q=searchedTerm" rel="alternate" />
  <id>http://localhost/opensearch/search?q=searchedTerm</id>
  <updated>2018-10-19T17:38:16Z</updated>
  <entry>
    <title>result1</title>
    <link href="http://localhost/result1" rel="alternate" />
    <updated>2018-10-19T17:38:16Z</updated>
    <id>tag:localhost:/result1</id>
    <summary type="html">description1</summary>
  </entry>
</feed>
"""

opensearch_descriptor_with_page_offset = """<?xml version="1.0" encoding="UTF-8"?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
  <ShortName>Third party app</ShortName>
  <Description>Third party app search engine</Description>
  <Url type="application/atom+xml" template="http://localhost:9991/opensearch/search?q={searchTerms}"
    indexOffset="io" pageOffset="po"/>
</OpenSearchDescription>
"""


class OpenSearchClientTest(asynctest.TestCase, TestCase):

    def setUp(self):

        self.opensearch_url_tokenizer_mock = Mock(spec=OpenSearchUrlTokenizer)
        self.opensearch_client = OpenSearchClient(self.opensearch_url_tokenizer_mock)
        self.opensearch_url_tokenizer_mock.resolve_search_url.return_value = \
            "http://localhost:9991/opensearch/search?q=searched"
        self.opensearch_url_tokenizer_mock.resolve_url.return_value = "http://result.url"

    @mocketize
    def test_query(self):
        Entry.single_register(
            Entry.GET,
            "http://localhost:9991/opensearch",
            body=opensearch_descriptor
        )

        Entry.single_register(
            httpretty.GET,
            "http://localhost:9991/opensearch/search?q=searched",
            body=atom_feed
        )

        # when

        async def main(f):
            with async_timeout.timeout(3):
                res = await self.opensearch_client.search_from_descriptor("http://localhost:9991/opensearch", "searched")

                # then
                expected_res = SearchResult(url="http://result.url", title="result1", description="description1")

                self.assertEqual([expected_res], res)
                self.opensearch_url_tokenizer_mock.resolve_search_url.assert_called_once_with(
                    "http://localhost:9991/opensearch",
                    "http://localhost:9991/opensearch/search?q={searchTerms}",
                    "searched",
                    0,
                    0
                )

                self.opensearch_url_tokenizer_mock.resolve_url.assert_called_once_with(
                    "http://localhost:9991/opensearch/search?q=searched",
                    "http://localhost/result1"
                )

        loop = asyncio.get_event_loop()
        loop.run_until_complete(main(loop))

    @mocketize
    def test_query_with_index_and_page_offset(self):
        Entry.single_register(
            Entry.GET,
            "http://localhost:9991/opensearch",
            body=opensearch_descriptor_with_page_offset
        )

        Entry.single_register(
            httpretty.GET,
            "http://localhost:9991/opensearch/search?q=searched",
            body=atom_feed
        )

        # when

        async def main(f):
            with async_timeout.timeout(3):
                res = await self.opensearch_client.search_from_descriptor("http://localhost:9991/opensearch", "searched")

                # then
                self.opensearch_url_tokenizer_mock.resolve_search_url.assert_called_once_with(
                    "http://localhost:9991/opensearch",
                    "http://localhost:9991/opensearch/search?q={searchTerms}",
                    "searched",
                    "io",
                    "po"
                )

        loop = asyncio.get_event_loop()
        loop.run_until_complete(main(loop))


class OpenSearchUrlTokenizerTest(TestCase):

    def setUp(self):
        self.url_tokenizer = OpenSearchUrlTokenizer()

    def test_resolve_search_url(self):
        """ Ensure term token is correctly replaced in an absolute opensearch url """
        # when
        search_url = self.url_tokenizer.resolve_search_url(
            "http://my.example.com/search/opensearch.xml", "http://my.example.com/q={searchTerms}", "toSearch"
        )

        # then
        self.assertEquals("http://my.example.com/q=toSearch", search_url)

    def test_resolve_relative_context_root_search_url(self):
        """ Ensure that a relative url expressed from the context root is correctly resolved"""
        # when
        search_url = self.url_tokenizer.resolve_search_url(
            "http://my.example.com/an/open/search/opensearch.xml", "/search?q={searchTerms}", "toSearch"
        )

        # then
        self.assertEquals("http://my.example.com/search?q=toSearch", search_url)

    def test_resolve_relative_context_root_search_url_with_double_slash(self):
        """ Ensure that a relative url expressed from the context root is correctly resolved"""
        # when
        search_url = self.url_tokenizer.resolve_search_url(
            "http://my.example.com/an/open/search/opensearch.xml", "//search?q={searchTerms}", "toSearch"
        )

        # then
        self.assertEquals("http://my.example.com/search?q=toSearch", search_url)

    def test_resolve_relative_search_url(self):
        """ Ensure that a relative url expressed relatively from the descriptor is correctly resolved """

    def test_resolve_relative_search_url(self):
        """ Ensure that a relative url expressed relatively from the descriptor is correctly resolved """
        # when
        search_url = self.url_tokenizer.resolve_search_url(
            "http://my.example.com/an/open/search/opensearch.xml", "search?q={searchTerms}", "toSearch"
        )

        # then
        self.assertEquals("http://my.example.com/an/open/search/search?q=toSearch", search_url)

    def test_resolve_url(self):
        """ Ensure that a correct absolute url is returned resolved from the descriptor url"""
        # when
        url_already_absolute = self.url_tokenizer.resolve_url(
            "http://my.example.com/os/opensearch.xml",
            "http://serve.com/result"
        )

        url_from_context_root = self.url_tokenizer.resolve_url("http://my.example.com/search/opensearch.xml", "/result")

        url_from_search_endpoint = self.url_tokenizer.resolve_url("http://my.example.com/search/endpoint", "result")

        # then
        self.assertEqual("http://serve.com/result", url_already_absolute)
        self.assertEqual("http://my.example.com/result", url_from_context_root)
        self.assertEqual("http://my.example.com/search/result", url_from_search_endpoint)
