from authlib.flask.oauth2 import ResourceProtector
from flask_restplus import Namespace, Resource, fields, marshal_with, inputs
from injector import inject
from flask import request, make_response, send_file
from flask_cors import CORS

import config
from app.logo.service import LogoService
from app.tools import request_tools
from app.tools.base_resource import BaseResource
from app.tools.request_tools import ResourceWithLinks, protected_resource, message, base_url_from_namespace

authorizations = {
    'Bearer': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

logo_api = Namespace(
    'logo', description="API managing logo", authorizations=authorizations)

logo_fields = logo_api.model('LogoLink', {
    'rel': fields.String(description="identifier of the link"),
    'href': fields.String(description="url of the link")
})

logo_model = logo_api.model('Logo', {
    'thumbnail': fields.String(description="admin Thumbnail"),
    'thumbnail_mime_type': fields.String(description='thumbnail_mime_type admin logo')
})


def to_logo_resource_link(res):
    r = ResourceWithLinks(res, logo_api)
    r.add_link("thumbnail", "/thumbnail")

    return r


@logo_api.route("/admin/thumbnail")
class LogoResource(Resource):

    @inject
    def __init__(self, logo_service: LogoService, resource_protector: ResourceProtector, api):
        self.logo_service = logo_service
        self.resource_protector = resource_protector
        self.api = api

    # @logo_api.marshal_with(logo_model, skip_none=True)
    # @protected_resource(role="admin")
    def get(self):
        content_type, thumbnail = self.logo_service.get_logo_thumbnail()

        if type(thumbnail) is str:
            resp = send_file(thumbnail, mimetype=content_type)
        else:
            resp = make_response(thumbnail)
            resp.content_type = content_type

        return resp

    @logo_api.doc(security='Bearer')
    @logo_api.marshal_with(logo_model, envelope="data", skip_none=True)
    @logo_api.doc(security=authorizations)
    @protected_resource(role="admin")
    def put(self):
        data = request.data
        content_type = request.content_type

        self.logo_service.set_logo_thumbnail(content_type, data)

        return message("Ok")
