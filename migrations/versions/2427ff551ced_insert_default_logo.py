"""insert default_logo

Revision ID: 2427ff551ced
Revises: c10839057732
Create Date: 2021-08-05 13:13:12.818228

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table, column
from sqlalchemy import String, Integer, Date
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2427ff551ced'
down_revision = 'c10839057732'
branch_labels = None
depends_on = None


def upgrade():
    logo_table = table('LOGO',
                       column('id', Integer),
                       column('thumbnail', sa.LargeBinary()),
                       column('thumbnail_mime_type', String)
                       )

    with open("app/logo/static/img/logo.png", "rb") as logoFile:
        blobLogo = logoFile.read()
    logoType = 'image/png'

    op.bulk_insert(logo_table,
                   [
                       {'id': 1, 'thumbnail': blobLogo,
                        'thumbnail_mime_type': logoType}
                   ]
                   )


def downgrade():
    pass
