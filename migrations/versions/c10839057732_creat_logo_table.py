"""creat logo table

Revision ID: c10839057732
Revises: 333435b6522d
Create Date: 2021-07-23 02:15:09.879645

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c10839057732'
down_revision = '333435b6522d'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('logo',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('thumbnail', sa.LargeBinary(), nullable=True),
                    sa.Column('thumbnail_mime_type', sa.String(
                        length=255), nullable=True),
                    sa.PrimaryKeyConstraint('id')
                    )


def downgrade():
    op.drop_table('logo')
